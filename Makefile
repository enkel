TRANS_FOLDER=enkel/translations
DOMAIN=default
NB_FOLDER=$(TRANS_FOLDER)/nb/LC_MESSAGES
EN_FOLDER=$(TRANS_FOLDER)/en/LC_MESSAGES
POT=$(TRANS_FOLDER)/$(DOMAIN).pot

doc:
	epydoc --config epydoc.html.cfg -v

clean:
	rm -f `find ./ -iname "*.pyc"`
	rm -rf apidoc/ docs/
	rm -rf dist/ build/ Enkel.egg-info/

pot:
	@echo "*** updating pot-file from sources"
	xgettext -o $(POT) --keyword="N_:1" `find enkel/ -name "*.py"`

merge:
	@echo "*** merging changes is pot-file into existing po-files"
	msgmerge -U $(NB_FOLDER)/$(DOMAIN).po $(POT) --suffix=.bak
	msgmerge -U $(EN_FOLDER)/$(DOMAIN).po $(POT) --suffix=.bak

potmerge: pot merge

mo:
	@echo "*** compiling po-files to *.mo"
	msgfmt -o $(NB_FOLDER)/$(DOMAIN).mo $(NB_FOLDER)/$(DOMAIN).po
	msgfmt -o $(EN_FOLDER)/$(DOMAIN).mo $(EN_FOLDER)/$(DOMAIN).po
