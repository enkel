NOTE: This library is still in ALPHA.


Getting started
===============
The examples use bash/zsh shell compatible commands.


Requirements
------------

If you want all the tests to pass install "lxml", "pysqlite",
and "setuptools". You do not need "pysqlite" if you use python2.5.


Run the testsuite
-----------------

Run the tests using (all should pass):
	~$ python setup.py test
You can use:
	~$ make clean
to remove the temporary files.


Build the apidocs
-----------------

Install ">=epydoc-3". And run:
	~$ make doc
If everything works you should be able to browse the docs by
pointing your browser to:
	~$ apidoc/index.html


Install the library in development mode
---------------------------------------

Setuptools can be used to install the library in development mode.
This actually just makes a link, so you can still use git without
having to reinstall.

Lets use ~/devpy as our install folder:
	~$ mkdir ~/devpy
	~$ export PYTHONPATH="$PYTHONPATH:$HOME/devpy"
	~$ export PATH="$PATH:$HOME/devpy"
	~$ python setup.py develop -d ~/devpy
You should of course add the exports to your .bashrc/.zshrc or
similar.

You can test the install like this:
	~$ cd
	~$ python -c "import enkel"
