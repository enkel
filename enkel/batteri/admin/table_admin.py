# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from gettext import NullTranslations

from enkel.sqldb.errors import IntegrityError
from enkel.sqldb.query import Asc
from enkel.model.formgen import AutoSort
from enkel.exml.form.formgen import Form
from enkel.error.http import Http404
from enkel.model import data, util
from enkel.url_manager import UrlManager
from enkel.model.field.interface import FieldValidationError, SMALL_FIELD
from enkel.wansgli.apptester import encode_query
from enkel.forminput import FormInput

from admin import AdminApp



N_ = lambda string: string


class TableAdmin(AdminApp):
	""" A L{enkel.sqldb.table.Table} administration app.

	@cvar TABLE: A L{enkel.sqldb.table.Table}.
	@cvar PER_PAGE: Number of items to show on each page when
			browsing the table.
	"""
	ACTIONS = dict(
		create = N_("Create"),
		delete = N_("Delete"),
		edit = N_("Edit"),
		save = N_("Save"),
		savenew = N_("Save"),
		browse = N_("Browse")
	)
	INDEX = ["create", "browse"]
	TABLE = None
	PER_PAGE = 10


	def __init__(self, env, w, id, action, label):
		self.form = FormInput(env)
		self.b_translate = NullTranslations().ugettext
		super(TableAdmin, self).__init__(env, w, id, action, label)


	def setup_form(self, form_model):
		""" Setup the form model used in L{edit} and L{create}.

		Override this function to customize the form:
			- add labels and tooltips
			- change the layout

		@param form_model: A L{enkel.model.formgen.FormModel}
				initialized with a Manip for the table model.
		@return: A layout as defined
				L{here <enkel.model.formgen.Form.set_layout>}
		"""
		return AutoSort(form_model)


	def create_form(self, manip, errors=[], new=False):
		a = "save"
		if new:
			a = "savenew"
		url = UrlManager(self.env)
		url.path[-1] = a
		form = Form(url.create(), N_(u"Save"))

		fm = FormModel(manip)
		fm.set_errors(errors, self.b_translate)
		if not new:
			for fieldname in self.TABLE.primary_keys:
				getattr(fm, fieldname).read_only = True

		form.set_layout(*self.setup_form(fm))
		self.w.raw_node(form.create())


	def _pks_from_form(self):
		""" Read values for all primary-key fields from the form,
		convert them from string.

		@raise Http404: If import of a primary-key from the form fails.
		@return: A dict containing all primary-keys where the keys
				are fieldnames and the value  is the corresponding
				value from the form converted to correct type.
		"""
		pks = {}
		for pk, field in self.TABLE.iter_pks():
			value = field.from_unicode(self.form(pk, ""))
			try:
				field.validate(pk, value)
			except FieldValidationError, e:
				raise Http404(long_message="invalid pk: %s='%s'" % (pk, value))
			pks[pk] = value
		return pks



	def get_browse_fields(self, form_model):
		""" Get the fields (columns) used when browsing.

		Override this method to customize the columns in
		the browser page.

		@param form_model: A L{enkel.model.formgen.FormModel}.
		@return: A list of L{enkel.model.formgen.FormField} objects
				in the desired order. This implementation orders
				the fields automatically based on their WEIGHT.
		"""
		l = []
		for formfield in form_model:
			if formfield.field.WEIGHT < SMALL_FIELD:
				l.append(formfield)
		l.sort()
		return l


	############################
	# actions

	def browse(self):
		page = int(self.form.GET("page", "0"))
		offset = self.PER_PAGE * page
		n = 0

		browse_fields = self.get_browse_fields(
				FormModel(self.TABLE.manip()))

		self.w.start_element("p")
		self.w.start_element("table")


		# Add header
		self.w.start_element("hrow")
		for formfield in browse_fields:
			self.w.start_element("entry")
			self.w.text_node(formfield.label)
			self.w.end_element()
		for x in xrange(2):
			self.w.empty_element("entry")
		self.w.end_element()


		# Fill table with data
		for row in self.TABLE.select(
			offset=offset, limit=self.PER_PAGE,
			order_by=[Asc(self.TABLE.primary_keys[0])]
		):
			self.w.start_element("row")

			# add data

			for formfield in browse_fields:
				self.w.start_element("entry")
				self.w.text_node(row.get_unicode(formfield.name))
				self.w.end_element()


			# add "action" links

			params = [(pk, row.get_unicode(pk)) for pk in self.TABLE.primary_keys]

			self.w.start_element("entry")
			self.w.start_element("strong",
					href="edit?" + encode_query(params))
			self.w.text_node(N_("edit"))
			self.w.end_element(2)

			self.w.start_element("entry")
			self.w.start_element("a",
					href="delete?" + encode_query(params))
			self.w.text_node(N_("delete"))
			self.w.end_element(2)

			self.w.end_element() # close <row>
			n += 1

		self.w.end_element() # end <table>


		# Add the next/previous links

		url = UrlManager(self.env)
		next = None
		prev = None

		if page > 0:
			url.query_string = "page=%d" % (page - 1)
			self.w.start_element("a", href=url.create())
			self.w.text_node(N_("previous"))
			self.w.end_element()

		if n == self.PER_PAGE:
			url.query_string = "page=%d" % (page + 1)
			self.w.start_element("a", href=url.create())
			self.w.text_node(N_("next"))
			self.w.end_element()

		self.w.end_element() # end <p>


	def create(self):
		manip = self.TABLE.manip()
		self.create_form(manip, new=True)


	def edit(self):
		pks = self._pks_from_form()
		manip = self.TABLE.select_one(**pks)
		if not manip:
			raise Http404()

		self.create_form(manip)


	def save(self, new=False):
		manip = util.forminput_to_manip(self.TABLE.model, self.form.POST)
		errors = manip.info_validate()
		if errors:
			self.create_form(manip, errors, new=new)
		else:
			try:
				if new:
						self.TABLE.insert(manip)
				else:
					self.TABLE.update(manip)
			except IntegrityError:
				self.w.start_element("p")
				self.w.start_element("strong")
				self.w.text_node("Integrity error")
				self.w.end_element(2)
				self.create_form(manip, error=error)
			self.TABLE.adapter.commit()
			self.w.start_element("p")
			self.w.text_node(N_("save successful"))
			self.w.end_element()


	def savenew(self):
		self.save(new=True)


	def delete(self):
		pks = self._pks_from_form()
		self.TABLE.qry_delete(**pks)
		self.w.start_element("p")
		self.w.text_node(N_("delete successful"))
		self.w.end_element()
		self.TABLE.adapter.commit()
