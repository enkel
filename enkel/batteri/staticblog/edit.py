# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from os import listdir, remove
from os.path import join, isfile
from re import compile, DOTALL, VERBOSE
import codecs

from enkel.forminput import FormInput
from enkel.batteri.admin.admin import AdminApp
from enkel.model.field.xmlf import XmlField
from enkel.model.util import ModelData, forminput_to_manip
from enkel.model import data
from enkel.exml.formgen import Form
from enkel.model.field import base, reg
from enkel.exml.info import XMLNS_MARKUP
from enkel.xmlutils.writer import XmlWriter
from enkel.wansgli.apptester import encode_query

from create_blog import create_blog, parse_config, StaticblogError
from validate_post import validate_post_field


XMLNS_STATICBLOG = "http://enkel.sourceforge.net/xml/staticblog"
ENCODING = "utf-8"
N_ = lambda string: string



format = \
"""<post xmlns="%(XMLNS_STATICBLOG)s">
	<summary/>
	<section xmlns="%(XMLNS_MARKUP)s"
			xmlns:s="%(XMLNS_STATICBLOG)s">
	<h>heading</h>
	%%s
	</section>
</post>""" % vars()
MODEL = dict(
	id = reg.IdString(required=False),
	heading = base.String(45),
	summary = base.String(500),
	tags = base.String(500, required=False),
	post = XmlField(format=format, offset=5,
			validate=validate_post_field)
)



POST_PARSE_PATT = compile(
""".*?
<post.*?>
.*?
	<summary>(?P<summary>.*?)</summary>
	(?P<tags>.*?)
	<section.*?>
		.*?
		<h>(?P<heading>.*?)</h>
		(?P<post>.*)
	</section>
.*?
</post>""", DOTALL|VERBOSE)

TAGS_PARSE_PATT = compile("<tag>(.*?)</tag>", DOTALL)
UNIVERSAL_NEWLINE_PATT = compile(r"(\r\n|\r)")




class StaticBlogEdit(AdminApp):
	ACTIONS = dict(
		browse = N_("Browse"),
		create = N_("Create"),
		edit = N_("Edit"),
		save = N_("Save"),
		delete = N_("Delete"),
		rebuild = N_("Rebuild blog")
	)
	INDEX = ["browse", "create", "rebuild"]


	def __init__(self, env, w, id, action, label,
				configfile):
		"""
		@param configfile: Path to the configfile for the blog.
		"""
		self.posts_folder, self.theme_folder, self.process_command, \
			self.sync_command = parse_config(configfile)
		super(StaticBlogEdit, self).__init__(
				env, w, id, action, label)

	def browse(self):
		self.w.start_element("p")
		self.w.start_element("table")

		for id in listdir(self.posts_folder):
			self.w.start_element("row")

			self.w.start_element("entry")
			self.w.text_node(id)
			self.w.end_element()

			params = [("id", id)]
			self.w.start_element("entry")
			self.w.start_element("strong",
					href="edit?" + encode_query(params))
			self.w.text_node(N_("edit"))
			self.w.end_element(2)

			self.w.start_element("entry")
			self.w.start_element("a",
					href="delete?" + encode_query(params))
			self.w.text_node(N_("delete"))
			self.w.end_element(2)

			self.w.end_element() # </row>

		self.w.end_element() # </table>


	def _edit(self, manip, validate=False):
		form = Form("save", "Save", method="post")
		form["e_"] = ModelData(manip)
		if validate:
			form["e_"].validate()
		self.w.raw_node(form.create())

	def create(self):
		manip = data.Manip(MODEL)
		self._edit(manip)

	def edit(self):
		""" Open the post and parse it into a Manip using
		L{POST_PARSE_PATT} and L{TAGS_PARSE_PATT}. Then create
		a form from the manip using exml formgen. """

		id = FormInput(self.env).GET.getfirst("id")
		if not id:
			raise Exception("no id")
		path = join(self.posts_folder, id)
		post = codecs.open(path, "rb", ENCODING).read()

		r = POST_PARSE_PATT.match(post)
		tags = [m.group(1).strip() \
				for m in TAGS_PARSE_PATT.finditer(r.group("tags"))]

		manip = data.Manip(MODEL)
		manip.id = id
		manip.summary = r.group("summary").strip()
		manip.heading = r.group("heading").strip()
		manip.post = r.group("post").strip()
		manip.tags = ", ".join(tags)
		self._edit(manip)


	def save(self):
		manip = forminput_to_manip(MODEL, FormInput(self.env), "e_")
		try:
			manip.validate()
		except base.FieldValidationError:
			self._edit(manip, validate=True)
		else:
			p = XmlWriter(pretty=True)
			p.pi("xml", version="1.0", encoding=ENCODING)
			p.start_element("post", xmlns=XMLNS_STATICBLOG)

			p.start_element("summary")
			p.text_node(manip.summary)
			p.end_element()

			for tag in manip.tags.split(","):
				p.start_element("tag")
				p.text_node(tag.strip())
				p.end_element()

			p.start_element("section", xmlns=XMLNS_MARKUP,
					attrdict={"xmlns:s": XMLNS_STATICBLOG})
			p.start_element("h")
			p.text_node(manip.heading)
			p.end_element()
			p.raw_node(manip.post)
			p.end_element()

			p.end_element() # </post>

			path = join(self.posts_folder, manip.id)

			post = UNIVERSAL_NEWLINE_PATT.sub(r"\n", p.create())
			codecs.open(path, "wb", ENCODING).write(post)

			self.w.start_element("p")
			self.w.text_node(N_("Save successful"))
			self.w.end_element()


	def delete(self):
		id = FormInput(self.env).GET.getfirst("id")
		if not id:
			raise Exception("no id")
		path = join(self.posts_folder, id)
		remove(path)
		self.w.start_element("p")
		self.w.text_node(N_("Delete successful"))
		self.w.end_element()


	def rebuild(self):
		self.w.start_element("p")
		try:
			create_blog(self.posts_folder, self.theme_folder,
					self.process_command, self.sync_command)
		except StaticblogError, e:
			self.w.text_node(str(e))
		else:
			self.w.text_node(N_("Rebuild successful"))
		self.w.end_element()
