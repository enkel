"""
@var XMLNS_MARKUP: The namespace used by the exml markup language.
@var XMLNS_FORM: The namespace used by the exml form language.
"""
XMLNS_MARKUP = "http://enkel.sourceforge.net/xml/markup"
XMLNS_FORM = "http://enkel.sourceforge.net/xml/form"
