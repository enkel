# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from xml.sax import parseString, handler
from enkel.xmlutils.writer import XmlWriter
from re import compile



class XhtmlToMarkupHandler(handler.ContentHandler):
	HEAD_PATT = compile("h[1-6]")

	INLINE = dict(
		b = "strong",
		strong = "strong",
		em = "em",
		i = "em",
		code = "code",
		a = "a",
		img = "image"
	)

	CONTAINS_INLINE = set(["li", "p", "th", "td"])
	WHITESPACE = compile("\s+")

	def __init__(self, pretty):
		self.buf = []
		self.out = XmlWriter(pretty=pretty)
		self._out = self.out
		self.levels = []
		self.use_content = False
		self.xmllevel = 0
		self.bodylevel = None
		self.in_body_para = False
		self.inline_element_level = None
		self.in_ul_or_ol = False
		self.in_table = False
		self.in_tr = False
		self.inline_container_level = None


	def start_body_para(self):
		self.in_body_para = True
		self._out = XmlWriter()

	def end_body_para(self):
		p = self._out.create().strip()
		self.in_body_para = False
		self._out = self.out
		if p:
			self._out.start_element("p")
			self._out.raw_node(p)
			self._out.end_element()


	def startElement(self, name, attr):
		name = name.lower()
		self.xmllevel += 1

		if self.in_body_para and not name in self.INLINE:
			self.end_body_para()

		self.use_content = True
		if name in self.INLINE:
			if self.inline_element_level == None:
				self.inline_element_level = self.xmllevel
				if self.bodylevel != None and \
						self.xmllevel == self.bodylevel + 1 and \
						not self.in_body_para:
					self.start_body_para()
				if name == "img":
					self._out.empty_element("image",
							href=attr.get("src", ""))
				else:
					att = {}
					if name == "a":
						att["href"] = attr.get("href", "")
					self._out.start_element(self.INLINE[name], **att)
		elif self.inline_container_level != None:
			pass
		elif self.HEAD_PATT.match(name):
			level = int(name[1])
			if self.levels:
				if level > self.levels[-1]:
					self.levels.append(level)
				else:
					while level < self.levels[-1]:
						self.levels.pop()
						self._out.end_element() # </section>
					self._out.end_element() # </section>
			else:
				self.levels.append(level)
			self._out.start_element("section")
			self._out.start_element("h")
		elif name == "p":
			self._out.start_element("p")

		elif name == "ul" or name == "ol":
			self._out.start_element("p")
			self._out.start_element(name)
			self.in_ul_or_ol = True
		elif self.in_ul_or_ol and name == "li":
			self._out.start_element("li")

		elif name == "table":
			self._out.start_element("p")
			self._out.start_element("table")
			self.in_table = True
			self.use_content = False
		elif self.in_table and name == "tr":
			self._out.start_element("row")
			self.use_content = False
			self.in_tr = True
		elif self.in_tr and (name == "td" or name == "th"):
			att = {}
			colspan = attr.get("colspan")
			if colspan:
				att["colspan"] = colspan
			rowspan = attr.get("rowspan")
			if rowspan:
				att["rowspan"] = rowspan
			self._out.start_element("entry", **att)

		elif name == "body":
			self.bodylevel = self.xmllevel
		else:
			self.use_content = False

		if self.inline_container_level == None and \
				name in self.CONTAINS_INLINE:
			self.inline_container_level = self.xmllevel



	def characters(self, content):
		content = self.WHITESPACE.sub(" ", content)
		if self.xmllevel == self.bodylevel:
			self.use_content = True
			if not self.in_body_para:
				self.start_body_para()
		if self.use_content:
			self._out.text_node(content)


	def endElement(self, name):
		name = name.lower()

		if self.inline_container_level == self.xmllevel and \
				name in self.CONTAINS_INLINE:
			self.inline_container_level = None

		self.use_content = False
		if name in self.INLINE:
			self.use_content = True
			if self.inline_element_level == self.xmllevel:
				self.inline_element_level = None
				if not name == "img":
					self._out.end_element()
		elif self.inline_container_level != None:
			pass
		elif self.HEAD_PATT.match(name):
			self._out.end_element() # </h>
		elif name == "p":
			self._out.end_element() # </p>

		elif name == "ul" or name == "ol":
			self.in_ul_or_ol = False
			self._out.end_element(2)
		elif self.in_ul_or_ol and name == "li":
			self._out.end_element()

		elif name == "table":
			self.in_table = False
			self._out.end_element(2)
		elif self.in_table and name == "tr":
			self.in_tr = False
			self._out.end_element()
		elif self.in_tr and (name == "td" or name == "th"):
			self._out.end_element()
		elif name == "body":
			self.bodylevel = None

		self.xmllevel -= 1


	def endDocument(self):
		if self.in_body_para:
			self.end_body_para()



def xhtml_to_markup(xhtml, pretty=False):
	h = XhtmlToMarkupHandler(pretty)
	parseString(xhtml, h)
	return h.out.create()
