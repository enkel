# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

""" Simplified form input parsing similare to cgi.FieldStorage
(uses enkel.wansgli.formparse) """


from enkel.wansgli.formparse import parse_query_string, parse_multipart,\
		FormParams
import settings



class FormInputError(Exception):
	pass

class FormInputMethodError(FormInputError):
	pass

class FormInputClientError(FormInputError):
	def __init__(self, msg):
		msg = "Client bug/error: " + msg
		FormInputError.__init__(self, msg)



class Any(object):
	""" When you have data in more than one
	L{enkel.wansgli.formparse.FormParams}, and do not care from
	which FormParams the data is retrived, you can use Any.
	It implements a subset of the functions provided by
	FormParams. """
	def __init__(self, *form_params):
		"""
		@param form_params: tuple of L{enkel.wansgli.formparse.FormParams}
				sorted in the preferred order.
		"""
		self.form_params = form_params

	def get(self, key, default=None):
		""" Get the list of parameters from the first FormParams
		containing "key", or return "default".
		"""
		for p in self.form_params:
			if key in p:
				return p[key]
		return default

	def getfirst(self, key, default=None):
		""" Get the first item in the list of parameters from the
		first FormParams containing "key", or return "default".
		"""
		v = self.get(key)
		if v == None:
			return default
		else:
			return v[0]

	def __call__(self, key, default=None):
		""" Alias for L{getfirst} """
		return self.getfirst(key, default)

	def __contains__(self, key):
		""" Check if "key" is in any of the FormParams. """
		for p in self.form_params:
			if key in p:
				return True
		else:
			return False

	def __repr__(self):
		b = ""
		for p in self.form_params:
			b += repr(p)
		return b


class FormInput(object):
	"""
	>>> from cStringIO import StringIO
	>>> from enkel.wansgli.apptester import encode_multipart


	Using GET method
	================
		>>> env = dict(
		... 	QUERY_STRING = "name=john&id=8812&x=10&x=20",
		... 	REQUEST_METHOD = "GET"
		... )
		>>> f = FormInput(env)
		>>> f.GET
		{u'x': [u'10', u'20'], u'name': [u'john'], u'id': [u'8812']}
		>>> f.POST
		{}
		>>> f.GET.get("x")
		[u'10', u'20']
		>>> f.GET("x")
		u'10'
		>>> f.GET("x") == f.GET.getfirst("x")
		True


	Using POST method
	=================
		>>> q = "a=10&a=20&name=peter"
		>>> env = dict()
		>>> env["REQUEST_METHOD"] = "POST"
		>>> env["wsgi.input"] = StringIO(q)
		>>> env["CONTENT_LENGTH"] = len(q)
		>>> env["CONTENT_TYPE"] = "application/x-www-form-urlencoded"

		>>> f = FormInput(env)
		>>> f.POST
		{u'a': [u'10', u'20'], u'name': [u'peter']}
		>>> f.POST.get("name")
		[u'peter']
		>>> f.POST("a")
		u'10'


	any and default
	===============
		>>> env = dict(
		... 	QUERY_STRING = "name=john&id=8812&x=10&x=20",
		... 	REQUEST_METHOD = "GET"
		... )

		any is used if you do not care about the method.

		>>> f = FormInput(env)
		>>> f.any
		{u'x': [u'10', u'20'], u'name': [u'john'], u'id': [u'8812']}{}
		>>> f.any("name")
		u'john'
		>>> f.any.getfirst("x")
		u'10'

		defaults can be used just like with dict.get()

		>>> f.any("does not exist", 1000)
		1000


	Testing multipart request
	=========================
		>>> params = [
		... 	("myfile", "this is a test", "myfile.png", "image/png"),
		... 	("x", "10"),
		... 	("y", "20", "y1.txt"),
		... 	("y", "30", "y2.txt")
		... ]
		>>> boundary, q = encode_multipart(params)

		>>> env = dict()
		>>> env["REQUEST_METHOD"] = "POST"
		>>> env["wsgi.input"] = StringIO(q)
		>>> env["CONTENT_LENGTH"] = len(q)
		>>> env["CONTENT_TYPE"] = "multipart/form-data; boundary=%s" % boundary

		>>> f = FormInput(env)

		>>> f.files.getfirst("myfile").read()
		'this is a test'
		>>> f.files.getfirst("myfile").filename
		'myfile.png'
		>>> f.files("myfile").content_type
		'image/png'


	Testing unicode support
	=======================
		Note that we specify encoding="utf-8", but this is not
		really required as utf-8 is the default.

		>>> env = {
		... 	"REQUEST_METHOD": "GET",
		... 	"QUERY_STRING": u"name=\u00e5ge".encode("utf-8")
		... }
		>>> f = FormInput(env, encoding="utf-8")
		>>> f.GET.getfirst("name") == u"\u00e5ge"
		True


	@ivar GET: L{enkel.wansgli.formparse.FormParams} object containing
			all variables retrived from a GET request. All values
			are unicode strings.
	@ivar POST: L{enkel.wansgli.formparse.FormParams} object containing all
			variables retrived from a POST request. All values are
			unicode strings.
	@ivar any: L{Any} object containing all variables
			retrived from a POST or GET request. All values are strings.
	@ivar files: L{enkel.wansgli.formparse.FormParams} object containing all
			files retrived from a multipart POST request. All values are
			L{enkel.wansgli.formparse.MultipartFile} objects.
	"""

	def __init__(self, env, encoding=settings.encoding):
		self.env = env
		self.method = env["REQUEST_METHOD"]
		self.encoding = encoding
		self.GET = FormParams()
		self.POST = FormParams()
		self.files = FormParams()

		if self.method == "POST":
			self._parse_post()
		elif self.method == "GET":
			self._parse_get()

		self.any = Any(self.GET, self.POST)


	def _parse_get(self):
		s = self.env.get("QUERY_STRING", "")
		self.GET = parse_query_string(s, self.encoding)

	def _parse_post(self):

		# error check and gather required env variables
		try:
			length = long(self.env["CONTENT_LENGTH"])
		except KeyError:
			raise FormInputClientError(
				"POST request without a CONTENT_LENGTH header.")
		except ValueError:
			raise FormInputClientError(
				"CONTENT_LENGTH header could not be converted to int.")
		try:
			content_type = self.env["CONTENT_TYPE"]
		except KeyError:
			raise FormInputClientError(
				"POST request without a CONTENT_TYPE header")


		# check the content-type and parse body accordingly
		if content_type == "application/x-www-form-urlencoded":
			body = self.env["wsgi.input"].read(length)
			self.POST = parse_query_string(body, self.encoding)
		elif content_type.startswith("multipart/form-data"):
			self.files, self.POST = parse_multipart(content_type,
					length, self.env["wsgi.input"], self.encoding)


	def get(self, name, default):
		""" Alias for any.get(name, default). """
		return self.any.get(name, default)

	def getfirst(self, name, default):
		""" Alias for any.getfirst(name, default). """
		return self.any.getfirst(name, default)

	def __call__(self, name, default=None):
		""" Alias for getfirst(name, default). """
		return self.getfirst(name, default)



def suite():
	import doctest
	return doctest.DocTestSuite()

if __name__ == "__main__":
	from enkel.wansgli.testhelpers import run_suite
	run_suite(suite())
