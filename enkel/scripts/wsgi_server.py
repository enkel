#!/usr/bin/env python

from optparse import OptionParser, OptionGroup
from sys import exit

from enkel.wansgli.server import WsgiServer
from enkel.wansgli.demo_apps import simple_app
from enkel.wansgli.utils import import_attr



THREAD, FORK, WAIT, ONCE = range(4)

parser = OptionParser(usage="%prog <app>")
parser.set_description(
"""Where <app> is the python-path to a WSGI app. Note that this
application is ment for testing only, not as a full blown wsgi
server.""")

parser.add_option("--host", dest="host", default="", metavar="localhost",
		help="Host to bind to. Defaults to localhost.")
parser.add_option("-p", "--port", dest="port", type="int",
		default=80, metavar="80",
		help="Port to bind to. Note that lower "\
			"ports (usually <~ 1000) can only be used by the "\
			"administrator user.")
parser.add_option("--debug", dest="debug", default=False, action="store_true",
		help="Run in debug mode. Full error traceback's will be printed "\
				"to stdout.")

m = OptionGroup(parser, "method")
m.set_description("The method used to handle request. Defaults to --once.")
m.add_option("--thread", dest="method",
		action="store_const", const=THREAD,
		help="create a new thread per request.")
m.add_option("--fork", dest="method",
		action="store_const", const=FORK,
		help="fork a new process per request.")
m.add_option("--wait", dest="method",
		action="store_const", const=WAIT,
		help="Wait for a request to finish before handling the next.")
m.add_option("--once", dest="method",
		action="store_const", const=ONCE,
		help="Handle one request and finish.")
parser.add_option_group(m)

opt, args = parser.parse_args()

if len(args) < 1:
	parser.print_help()
	exit(1)
app_path = args[0]


# decide how-to handle multiple connections
if opt.method == THREAD:
	me = "threading"
	from SocketServer import ThreadingMixIn
	class Server(ThreadingMixIn, WsgiServer):
		multithread = True
elif opt.method == FORK:
	me = "forking"
	from SocketServer import ForkingMixIn
	class Server(ForkingMixIn, WsgiServer):
		multiprocess = True
elif opt.method == "ONCE":
	class Server(WsgiServer):
		run_once = True
else:
	me = "wait"
	Server = WsgiServer


# run server
try:
	app = import_attr(app_path)
except Exception, e:
	print "Failed to import the request WSGI application (%s)." % app_path
	print e
else:
	print "** starting server. use <ctrl-c> to quit"
	httpd = Server(app, port=opt.port, debug=opt.debug,
			server_info="WSGI %s server" % me,)
	if opt.method == ONCE:
		httpd.handle_request()
	else:
		httpd.serve_forever()
