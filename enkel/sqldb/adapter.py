# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class Adapter(object):
	""" Database adapter interface.

	@ivar connection: A PEP249 compatible connection object.
	@ivar cursor: A PEP249 compatible cursor object. Must support
		these variables/functions as specified in the PEP:
			- description
			- close
			- execute
			- fetchone
			- fetchall
			- fetcmany
	@ivar echo: if True, print all sql queries to stdout.
	@warning: All PEP249 modules used to implement this interface
			must have threadsafety >= 1 if you it is to be thread-
			safe.
	"""
	def commit(self):
		""" Commit transaction. """
		self.connection.commit()
	def rollback(self):
		""" Rollback transaction. """
		self.connection.rollback()
	def close(self):
		""" Close connection to database. """
		self.connection.close()
	def new_cursor(self):
		""" Returns a db-api cursor object. """
		return self.connection.cursor()

	def register(self, *tables):
		""" Register tables with the adapter. Only after a table
		has been registered can any database operations be performed
		on it.

		Registration can also be done using L{table.Table.set_adapter}.

		@param tables: A list of L{table.Table} objects.
		"""
		for table in tables:
			table.set_adapter(self)

	def drop_table(self, table):
		""" Drop/delete a table.

		Rollback from this operation does not work, and it will
		automatically commit any pending transactions before
		deleting the table.

		@param table: A L{table.Table} instance.
		"""
		raise NotImplementedError()

	def create_table(self, table):
		""" Create a table.

		Rollback from this operation does not work, and it will
		automatically commit any pending transactions before
		creating the table.

		@param table: A L{table.Table} instance.
		"""
		raise NotImplementedError()

	def execute(self, query, params=None, cursor=None):
		""" Execute the given query.

		If L{echo} is True, the queries and parameters is
		printed to standard output, formatted to suite the
		adapter.

		@param query: A sql query.
		@param params: A list of parameters or None.
		@param cursor: A PEP249 compatible cursor object. Defaults to
				L{cursor}.
		"""
		raise NotImplementedError()

	def insert(self, table, manip, ignore=[]):
		""" Insert manip/row into the database.

		Takes care of inserting into both the table and
		the related many-to-many tables.

		@param table: A L{table.Table} instance.
		@param manip: A L{enkel.model.data.Manip}
				for the model defining the table.
		@param ignore: A list of fieldnames to skip/ignore when
				inserting.
		"""
		raise NotImplementedError()

	def update(self, table, manip, mm_add=[], ignore=[]):
		""" Update manip/row.

		Takes care of updating both the table and the related
		many-to-many tables.

		@param table: A L{table.Table} instance.
		@param manip: A L{enkel.model.data.Manip}
				for the model defining the table.
		@type mm_add: List of fieldnames
		@param mm_add: Add the values of the given many-to-many fields
				instead of replacing.
		@param ignore: A list of fieldnames to skip/ignore updating.
		"""
		raise NotImplementedError()

	def delete(self, table, manip):
		""" Delete manip/row from the database.

		Removes any related record in many-to-many tables before
		removing the row.

		@param table: A L{table.Table} instance.
		@param manip: A L{enkel.model.data.Manip}
				for the model defining the table.
		"""
		raise NotImplementedError()

	def qry_delete(self, table, *ops, **named_ops):
		""" Delete all rows matching the given query.

		Searches for rows using L{select}, and deletes using
		L{delete}.

		Arguments are the same as for L{select}.
		"""
		for m in self.select(table, *ops, **named_ops).iterall():
			self.delete(table, m)


	def select(self, table, *ops, **named_ops):
		""" Simple select only involving one table (fast).

		I{ops} and I{named_ops} means the same as they do in
		L{query.WhereClause.__init__}.

		There is also one additional keyword argument, I{ignore},
		which is a list of field-names which you want to skip fetching from
		the database. This can speed things up alot if you have unrequired
		TEXT fields or, in L{rselect}, foreign-key-fields which
		recurses deep.

		@return: a L{manip_fetcher.SimpleManipFetcher}.
		"""
		raise NotImplementedError()

	def select_one(self, table, *ops, **named_ops):
		""" Same as L{select}, but fetch only the first row found.
		@return: A L{enkel.model.data.Manip} or None if no match is found.
		"""
		raise NotImplementedError()


	def jselect(self, table, *ops, **named_ops):
		""" Select using SQL JOINs (a lot faster than L{rselect}).

		Parameters are the same as in L{select}.

		@warning: Using joins to select has one restriction.
				The table cannot have more than one foreign-key
				relationship to the same table (many-to-many
				relationships does not count).

		@return: a L{manip_fetcher.JoinManipFetcher}
		"""
		raise NotImplementedError()

	def jselect_one(self, table, *ops, **named_ops):
		""" Same as L{jselect}, but fetch only the first row found.
		@return: A L{enkel.model.data.Manip} or None if no match is found.
		"""
		raise NotImplementedError()


	def rselect(self, table, *ops, **named_ops):
		""" Select rows recursively (slow).

		Parameters are the same as in L{select}.

		@return: a L{manip_fetcher.RecursiveManipFetcher}
		"""
		raise NotImplementedError()

	def rselect_one(self, table, *ops, **named_ops):
		""" Same as L{rselect}, but fetch only the first row found.
		@return: A L{enkel.model.data.Manip} or None if no match is found.
		"""
		raise NotImplementedError()

	def from_db(self, field, value):
		""" Convert a value in a SELECT result row into the correct
		value for a field.

		This method does not concern itself with DatasourceField's and
		None values, as these are handled where the method is used.

		@param field: L{enkel.model.field.interface.Field} instance.
		@param value: the value to convert.
		"""
		raise NotImplementedError()
