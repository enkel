# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from enkel.model.field.base import Int, Long, FieldValidationError



class ValidateDbIntMixin(object):
	"""
	@cvar MIN: Lowest allowed value for the field.
	@cvar MAX: Largest allowed value for the field.
	"""
	MAX = None
	MIN = None
	def min_max_validate(self, fieldname, value):
		"""
		@raise FieldValidationError: If value in not between L{MIN}
				and L{MAX}.
		"""
		if not self.required and value == None:
			return
		if not (value >= self.MIN and value <= self.MAX):
			raise FieldValidationError(fieldname, value,
				"must be between %s and %s" % (self.MIN, self.MAX))

class DbSmallInt(Int, ValidateDbIntMixin):
	""" A 16 bit signed integer field. """
	MAX = 2**15 - 1
	MIN = -MAX
	def validate(self, fieldname, value):
		"""
		@raise FieldValidationError: If
				L{enkel.model.field.base.Int.validate} or
				L{ValidateDbIntMixin.min_max_validate} fails.
		"""
		super(DbSmallInt, self).validate(fieldname, value)
		self.min_max_validate(fieldname, value)
	

class DbInt(Long, ValidateDbIntMixin):
	""" A 32 bit signed integer field. """
	MAX = 2**31 - 1
	MIN = -MAX
	def validate(self, fieldname, value):
		"""
		@raise FieldValidationError: If
				L{enkel.model.field.base.Long.validate} or
				L{ValidateDbIntMixin.min_max_validate} fails.
		"""
		super(DbInt, self).validate(fieldname, value)
		self.min_max_validate(fieldname, value)

class DbLong(DbInt):
	""" A 64 bit signed integer field. """
	MAX = 2**63 - 1
	MIN = -MAX



class AutoincPk(DbLong):
	WEIGHT = 50
	""" An auto-incrementing integer field only for use
	as database-table primary-key.

	It can only be used in tables with exactly one primary
	key.

	Technical info
	==============
		Autoincrementing primary-keys are implemented differentry
		in most database engines. It should be implemented as
		a combination SQL SEQUENCE and a SQL TRIGGER. And if all
		databses supported this feature, this interface would also
		support a general sequence type. But unfortunatly we have
		to restrict us to autoincrementing primary-keys.
	"""
	required = False
