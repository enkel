# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class AdapterError(Exception):
	""" Parent class for all database adapter errors. """
class DataError(AdapterError):
	""" Exception raised for errors that are due to problems with
	the processed data like division by zero, numeric value
	out of range, etc. """
class OperationalError(AdapterError):
	""" Exception raised for errors that are related to the
	database's operation and not necessarily under the control
	of the programmer. E.g. an unexpected disconnect occurs,
	the data source name is not found, a transaction could not
	be processed, a memory allocation error occurred during
	processing, etc. """
class IntegrityError(AdapterError):
	""" Exception raised when the relational integrity of the
	database is affected. Like when a foreign key check fails
	or an existing row is inserted.
	"""
class InternalError(AdapterError):
	""" Exception raised when the database encounters an internal
	error, e.g. the cursor is not valid anymore, the
	transaction is out of sync, etc. """
class ProgrammingError(AdapterError):
	""" Exception raised for programming errors, e.g. table not
	found or already exists, syntax error in the SQL
	statement, wrong number of parameters specified, etc.  """


class TableStructureError(Exception):
	""" Raised when a table is defined in an illegal way. """
