# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from enkel.model.data import Manip
from enkel.model.ds import DatasourceField



class ManyToManyFetcher(object):
	def __init__(self, table, source_value):
		self.table = table
		self.source_value = source_value

	def __iter__(self):
		""" Alias for L{iter_simple}. """ 
		return self.iter_simple()

	def iter_simple(self, order_by=None, limit=None, offset=None):
		""" Iterate over the many-to-many table using
		L{table.Table.select}. Yields the value of the
		primary-key of the foreign-table at each iteration.

		The keyword-arguments has the same meaning as in
		L{query.WhereClause}.

		I{order_by} can only work on the column named "target",
		since we only query the many-to-many table, and "source"
		will be the same in all resulting rows.
		"""
		r = self.table.select(self.table.c.source==self.source_value,
				order_by=order_by, limit=limit, offset=offset,
				ignore=["source"])
		for row in r:
			yield row.target

	def iter_join(self, order_by=None, limit=None, offset=None):
		""" Iterate over the many-to-many table using
		L{table.Table.jselect}. Yields a L{enkel.model.data.Manip}
		for the related row in the foreign table at each iteration.
		The Manip is of the same type as the ones described
		in L{SimpleManipFetcher}.

		The keyword-arguments has the same meaning as in
		L{query.WhereClause}.
		"""
		r = self.table.jselect(self.table.c.source==self.source_value,
				order_by=order_by, limit=limit, offset=offset,
				ignore=["source"])
		for row in r:
			yield row.target

	def iter_recursive(self, order_by=None, limit=None, offset=None):
		""" Iterate over the many-to-many table using
		L{table.Table.rselect}. Yields a L{enkel.model.data.Manip}
		for the related row in the foreign table at each iteration.
		The Manip is complete just like the ones described in
		L{RecursiveManipFetcher}.

		The keyword-arguments has the same meaning as in
		L{query.WhereClause}.
		"""
		r = self.table.rselect(self.table.c.source==self.source_value,
				order_by=order_by, limit=limit, offset=offset,
				ignore=["source"])
		for row in r:
			yield row.target

	def __repr__(self):
		return "<ManyToManyFetcher: %r>" % [x for x in self.iter_simple()]



class ManipFetcher(object):
	""" ManipFetcher's and it's subclasses are responsible
	for converting the response from the database into
	L{enkel.model.data.Manip}'s.

	All subclasses has one thing in common. They do not fetch
	many-to-many fields. Instead all the information needed
	to retrive data from many-to-many fields are stored
	in a L{ManyToManyFetcher} object, which is returned as
	the value of a many-to-many field.
	"""
	def __init__(self, cursor, table):
		self.cursor = cursor
		self.table = table

	def _add_mm(self, m):
		for fieldname, mmtable in self.table.mmtables.iteritems():
			m[fieldname] = ManyToManyFetcher(mmtable,
					m[self.table.primary_keys[0]])


	def _row_converter(self, row):
		raise NotImplementedError()

	def __iter__(self):
		""" Iterate over the result using PEP249's fetchmany.
		@return: An iterator yielding L{enkel.model.data.Manip}'s.
		"""
		while True:
			result = self.cursor.fetchmany()
			if not result:
				break
			for row in result:
				yield self._row_converter(row)

	def iterall(self):
		""" Same as L{__iter__}, except it caches all data in memory.
		It uses PEP249's fetchall instead of fetchmany. """
		for row in self.cursor.fetchall():
			yield self._row_converter(row)

	def fetchone(self):
		""" Fetch only one row.
		@return: A L{enkel.model.data.Manip} on success or
				None on failure.
		"""
		row = self.cursor.fetchone()
		if row:
			return self._row_converter(row)
		else:
			return None

	def _from_db(self, field, value):
		if value == None:
			return None
		if isinstance(field, DatasourceField):
			return self._from_db(field.datasource.field, value)
		return self.table.adapter.from_db(field, value)



class SimpleManipFetcher(ManipFetcher):
	""" Yields L{enkel.model.data.Manip} with all fields as
	normal values, just as they are stored in the database. """
	def _row_converter(self, row):
		m = Manip(self.table.model)
		for x, value in zip(self.cursor.description, row):
			fieldname = x[0]
			field = self.table.model[fieldname]
			m[fieldname] = self._from_db(field, value)
		self._add_mm(m)
		return m



class JoinManipFetcher(ManipFetcher):
	""" Yields L{enkel.model.data.Manip}s with foreign-key values
	as Manip objects instead of normal values. But this only
	recurses one level. So a foreign-key-field Manip do
	not have Manip objects for its foreign-key fields. """
	def _row_converter(self, row):
		manips = {self.table.name: Manip(self.table.model)}

		for fieldname, field in self.table.iter_fks():
			ftable = field.datasource
			manips[ftable.name] = Manip(ftable.model)

		for x, value in zip(self.cursor.description, row):
			tablename, fieldname = x[0].split("_", 1)
			m = manips[tablename]
			field = m.model[fieldname]
			m[fieldname] = self._from_db(field, value)

		m = manips[self.table.name]
		for fieldname, field in self.table.iter_fks():
			ftable = field.datasource
			m[fieldname] = manips[ftable.name]
		self._add_mm(m)
		return m


class RecursiveManipFetcher(ManipFetcher):
	""" Yields complete L{enkel.model.data.Manip} objects for
	every resulting row. Unlike L{JoinManipFetcher}, foreign-key
	fields are traversed recursively until all data is retrived. """
	def _row_converter(self, row):
		m = Manip(self.table.model)
		for x, value in zip(self.cursor.description, row):
			fieldname = x[0]
			field = m.model[fieldname]
			m[fieldname] = self._from_db(field, value)
		self._add_mm(m)

		for fieldname, field in self.table.iter_fks():
			ftable = field.datasource
			m[fieldname] = ftable.rselect_one(
					ftable.get_pk_queryfield() == m[fieldname])

		return m
