# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import psycopg2

from enkel.model.field.base import Text, String, DateTime

from std_adapter import StdAdapter
from errors import *
from table import Table
from query import paramgen_format
from dbfields import AutoincPk


class PostgreSqlAdapter(StdAdapter):
	def __init__(self, *args, **kw):
		"""
		Parameters are forwarded to psycopg2.connect, except for
		the "dbencoding" keyword which specifies the encoding
		used in the databases registered with the adapter.
		"dbencoding" defaults to utf-8.
		"""
		self.echo = False
		self._dbencoding = kw.pop("dbencoding", "utf-8")
		self.connection = psycopg2.connect(*args, **kw)
		self.cursor = self.new_cursor()

	def paramgen(self, index):
		return paramgen_format(index)

	def _get_sqltype(self, fieldtype, fieldname, field):
		if isinstance(field, DateTime):
			return "TIMESTAMP"
		else:
			return super(PostgreSqlAdapter, self)._get_sqltype(
					fieldtype, fieldname, field)

	def _get_sqlfield(self, fieldtype, fieldname, field):
		""" Overridden to work with AutoincPk. """
		if fieldtype == Table.FT_PK and isinstance(field, AutoincPk):
			return "%s BIGSERIAL" % fieldname
		else:
			return super(PostgreSqlAdapter, self)._get_sqlfield(
					fieldtype, fieldname, field)


	def execute(self, query, params=None, cursor=None):
		cursor = cursor or self.cursor
		if self.echo:
			print query
			if params:
				print params
			print
		try:
			if params:
				cursor.execute(query, params)
			else:
				cursor.execute(query)
		except psycopg2.DataError, e:
			raise DataError(str(e))
		except psycopg2.OperationalError, e:
			raise OperationalError(str(e))
		except psycopg2.IntegrityError, e:
			raise IntegrityError(str(e))
		except psycopg2.InternalError, e:
			raise InternalError(str(e))
		except psycopg2.ProgrammingError, e:
			raise ProgrammingError(str(e))

	def create_table(self, table):
		cols = []
		fks = []
		for t, fieldname, field in table.iter_fields():
			if t == Table.FT_MM:
				continue
			if t == Table.FT_FK:
				ftable = field.datasource
				fks.append("FOREIGN KEY(%s) REFERENCES %s(%s)" % (
						fieldname, ftable.name, ftable.get_pk()))
			d = self._get_sqlfield(t, fieldname, field)
			cols.append(d)
		cols.append("PRIMARY KEY(%s)" % ",".join(table.primary_keys))
		if table.unique:
			cols.append("UNIQUE(%s)" % ",".join(table.unique))
		cols.extend(fks)
		qry = "CREATE TABLE %s (\n\t%s\n)" % (
				table.name, ",\n\t".join(cols))
		self.commit()
		self.execute(qry)
		self._create_indexes(table)
		self.commit()

	def insert(self, table, manip, ignore=[]):
		""" Overridden to work with AutoincPk. """
		pk = table.primary_keys[0]
		if isinstance(table.model[pk], AutoincPk) and manip[pk] == None:
			ignore.append(pk)
		super(PostgreSqlAdapter, self).insert(table, manip, ignore)

	def from_db(self, field, value):
		if isinstance(field, String) or isinstance(field, Text):
			return unicode(value, self._dbencoding)
		return value
