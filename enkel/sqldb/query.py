# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

def param_as_dictkey(index):
	""" Get the dictionary key used by L{param_as_dictkey},
	L{paramgen_pyformat} and L{paramgen_named} for a parameter
	at the given index in a parameter list. """
	return "p%d" % index

def params_as_dict(params):
	""" Convert a list of parameters to a dict suitable
	for use in db-api implementations using named or pyformat
	param-styles. """
	d = {}
	for i, v in enumerate(params):
		d[param_as_dictkey(i)] = v
	return d


def paramgen_interface(index):
	""" Parameter generator interface.

	The db-api supports 5 different methods of including parameters
	in queries in a transparent fashion. Parameters are included
	in the query using some sort of marker, like a "?" or ":myparam".
	This module unites these methods by provinding paramgen_* funtions
	and by using a list for parameters at all times. The list can be
	converted to a dictionary using L{params_as_dict} when needed.

	@param index: The index for the parameter in the parameter list.
	@return: A string with the properly formatted parameter.
	"""
	raise NotImplementedError()

def paramgen_qmark(index):
	""" documented L{here <paramgen_interface>}. """
	return "?"
def paramgen_numeric(index):
	""" documented L{here <paramgen_interface>}. """
	return ":%d" % (index + 1)
def paramgen_named(index):
	""" documented L{here <paramgen_interface>}. """
	return ":%s" % param_as_dictkey(index)
def paramgen_format(index):
	""" documented L{here <paramgen_interface>}. """
	return "%s"
def paramgen_pyformat(index):
	""" documented L{here <paramgen_interface>}. """
	return "%%(%s)s" % param_as_dictkey(index)


class PartOfExpression(object):
	def get_sql(self, table, paramgen, params):
		raise NotImplementedError()


class OneValueOp(PartOfExpression):
	def __init__(self, field, op, value):
		self.field = field
		self.op = op
		if isinstance(value, QueryField):
			self.value = value.full_name()
		else:
			self.value = value

	def get_sql(self, table, paramgen, params):
		params.append(self.value)
		return "%s %s %s" % (self.field.full_name(), self.op,
				paramgen(len(params)-1))

class LikeOp(OneValueOp):
	def get_sql(self, table, paramgen, params):
		params.append(self.value)
		return "%s %s %s ESCAPE '|'" % (self.field.full_name(), self.op,
				paramgen(len(params)-1))

class SimpleOp(PartOfExpression):
	def __init__(self, field, op):
		self.field = field
		self.op = op

	def get_sql(self, table, paramgen, params):
		return "%s %s" % (self.field.full_name(), self.op)

class InOp(PartOfExpression):
	def __init__(self, field, prefix, *values):
		self.field = field
		self.prefix = prefix
		self.values = values

	def get_sql(self, table, paramgen, params):
		v = []
		for value in self.values:
			params.append(value)
			v.append(paramgen(len(params)-1))
		o = "%s %sIN(%s)" % (self.field.full_name(), self.prefix, ",".join(v))
		return o

class BetweenOp(PartOfExpression):
	def __init__(self, field, prefix, a, b):
		self.field = field
		self.prefix = prefix
		self.a = a
		self.b = b

	def get_sql(self, table, paramgen, params):
		params.append(self.a)
		params.append(self.b)
		l = len(params)
		return "%s %sBETWEEN %s AND %s" % (self.field.full_name(),
				self.prefix, paramgen(l-2), paramgen(l-1))



class QueryField(object):
	""" Query fields represents a field in a mysql table that
	can be used to build queries.
	"""
	def __init__(self, name, table):
		self.name = name
		self.table = table

	def __eq__(self, value):
		return OneValueOp(self, "=", value)
	def __ne__(self, value):
		return OneValueOp(self, "!=", value)
	def __ge__(self, value):
		return OneValueOp(self, ">=", value)
	def __gt__(self, value):
		return OneValueOp(self, ">", value)
	def __le__(self, value):
		return OneValueOp(self, "<=", value)
	def __lt__(self, value):
		return OneValueOp(self, "<", value)

	def _escape(self, value):
		return value.replace("|", "||").replace("%", "|%").replace("_", "|_")
	def like(self, value):
		""" Find fields using pattern matching.

		The pattern is simple:
			- "%" matches zero or more characters.
			- "_" matches one character.
		You can use % and _ '|%' and '|_' (and '||').

		@warning: This yields a SQL "LIKE" query, which in some databases
				are case insensitive.
		"""
		return LikeOp(self, "LIKE", value)
	def startswith(self, value):
		""" Match all rows where the value of this field starts with I{value}.
		Uses L{like}, so the same warning applies here.
		"""
		return self.like(self._escape(value) + "%")
	def endswith(self, value):
		""" Match all rows where the value of this field ends with I{value}.
		Uses L{like}, so the same warning applies here.
		"""
		return self.like("%" + self._escape(value))
	def contains(self, value):
		""" Match all rows where the value of this field contains I{value}.
		Uses L{like}, so the same warning applies here.
		"""
		return self.like("%" + self._escape(value) + "%")

	def not_like(self, value):
		""" Match everything not matched by L{like}(value). """
		return LikeOp(self, "NOT LIKE", value)
	def not_startswith(self, value):
		""" Match everything not matched by L{startswith}(value). """
		return self.not_like(self._escape(value) + "%")
	def not_endswith(self, value):
		""" Match everything not matched by L{endswith}(value). """
		return self.not_like("%" + self._escape(value))
	def not_contains(self, value):
		""" Match everything not matched by L{contains}(value). """
		return self.not_like("%" + self._escape(value) + "%")

	def in_list(self, *values):
		""" Match all rows where this fields value matches a
		value in I{values}. """
		return InOp(self, "", *values)
	def not_in_list(self, *values):
		""" Math everything not matched by L{in_list}(*values). """
		return InOp(self, "NOT ", *values)

	def between(self, a, b):
		""" Match all rows where the value of this field is >= a and <= b. """
		return BetweenOp(self, "", a, b)
	def not_between(self, a, b):
		""" Match everything not matched by L{between}(a, b). """
		return BetweenOp(self, "NOT ", a, b)

	def isnull(self):
		""" Match every row where the value of this field is null/None. """
		return SimpleOp(self, "IS NULL")
	def not_null(self):
		""" Math everything not matched by L{isnull}(). """
		return SimpleOp(self, "IS NOT NULL")


	def full_name(self):
		""" Get the full name/path of this field. """
		return "%s.%s" % (self.table, self.name)


class OpList(PartOfExpression):
	""" A list of L{PartOfExpression} instances that is
	joined using L{REL}.


	Examples
	========
		>>> n = QueryField("name", "mytable")
		>>> a = QueryField("age", "mytable")

		Using QueryField's
		------------------
		>>> And(n == "John", a == 20).compile(None, paramgen_qmark)
		('mytable.name = ? AND mytable.age = ?', ['John', 20])
		>>> Or(n == "John", a == 20).compile(None, paramgen_qmark)
		('mytable.name = ? OR mytable.age = ?', ['John', 20])


		Using keywords
		--------------
		Keywords are converted to QueryField's and compared with the
		value using ==.

		>>> And(price="6.9$", title="Deadspawn", 
		... 	author="Brian Lumley").compile("books", paramgen_qmark)
		('books.price = ? AND books.author = ? AND books.title = ?', ['6.9$', 'Brian Lumley', 'Deadspawn'])

		Remember that the order of keywords cannot be guaranteed.


	@cvar REL: The separator used to join the list.
	"""
	REL = None
	def __init__(self, *ops, **named_ops):
		self.ops = list(ops)
		self.named_ops = named_ops

	def get_sql(self, table, paramgen, params, format="(%s)"):
		for key, value in self.named_ops.iteritems():
			self.ops.append(QueryField(key, table) == value)

		q = format % self.REL.join(
				[x.get_sql(table, paramgen, params)
						for x in self.ops])
		return q

	def compile(self, table, paramgen):
		params = []
		q = self.get_sql(table, paramgen, params, "%s")
		return q, params

class And(OpList):
	REL = " AND "
class Or(OpList):
	REL = " OR "




class OrderDirection(object):
	""" Represents a direction in a sql ORDER BY function.
	Use L{Asc} or L{Desc}, not this class directly. """
	D = None
	def __init__(self, field):
		"""
		@param field: A L{QueryField} or a fieldname (as string).
		"""
		self.field = field
	def compile(self, table):
		if isinstance(self.field, QueryField):
			f = self.field
		else:
			f = QueryField(self.field, table)
		return "%s %s" % (f.full_name(), self.D)

class Asc(OrderDirection):
	""" Order ascending. """
	D = "ASC"
class Desc(OrderDirection):
	""" Order descending. """
	D = "DESC"




class WhereClause(object):
	def __init__(self, table, paramgen, *ops, **named_ops):
		""" SQL WHERE clause generator.

		Example
		=======
			>>> r = QueryField("rating", "artists")
			>>> q, p = WhereClause("artists", paramgen_qmark,
			... 	r > 10, r <= 41,
			... 	band = "Korn",
			... 	order_by = [Asc("name"), Desc("age")],
			... 	limit = 5, offset = 3).compile()
			>>> q
			'WHERE artists.rating > ? AND artists.rating <= ? AND artists.band = ? ORDER BY artists.name ASC, artists.age DESC LIMIT 5 OFFSET 3'
			>>> p
			[10, 41, 'Korn']

		@param table: The name of the table.
		@param paramgen: A parameter generator following the L{paramgen_interface}.
		@param ops: The same as in L{OpList.__init__}.
		@param named_ops: The same as in L{OpList.__init__}, except there are 3
			keywords with special meaning:
				- limit: Limit the result to this number of rows.
				- offset: Offset the result by this number of rows.
				- order_by: A list of L{OrderDirection} instances.
		"""
		self.table = table
		self.paramgen = paramgen
		self.limit = named_ops.pop("limit", None)
		self.offset = named_ops.pop("offset", None)
		self.order_by = named_ops.pop("order_by", None)
		self.ops = ops
		self.named_ops = named_ops

	def compile(self):
		w, p = And(*self.ops, **self.named_ops).compile(self.table, self.paramgen)
		q = []
		if w:
			q.append("WHERE " + w)
		if self.order_by:
			o = ", ".join([x.compile(self.table) for x in self.order_by])
			q.append("ORDER BY %s" % o)
		if not self.limit == None:
			q.append("LIMIT %d" % self.limit)
		if not self.offset == None:
			q.append("OFFSET %d" % self.offset)
		return " ".join(q), p



def suite():
	import doctest
	return doctest.DocTestSuite()

if __name__ == "__main__":
	from enkel.wansgli.testhelpers import run_suite
	run_suite(suite())
