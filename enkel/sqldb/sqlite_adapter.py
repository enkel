# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from datetime import time
try:
	import sqlite3 as sqlite
except ImportError:
	from pysqlite2 import dbapi2 as sqlite

from enkel.model.field.base import Date, DateTime, Time

from std_adapter import StdAdapter
from errors import *
from table import Table
from query import paramgen_qmark
from dbfields import AutoincPk, DbLong, DbInt


def adapt_time(t):
	""" Adapter to support datetime.date insertion on TIME cols. """
	return t.strftime("%H:%M:%S")
sqlite.register_adapter(time, adapt_time)


class SqliteAdapter(StdAdapter):
	def __init__(self, dbpath):
		self.dbpath = dbpath
		self.echo = False
		self.connection = sqlite.connect(dbpath)
		self.connection.isolation_level = "DEFERRED"
		self.cursor = self.new_cursor()

	def paramgen(self, index):
		return paramgen_qmark(index)


	def execute(self, query, params=None, cursor=None):
		cursor = cursor or self.cursor
		if self.echo:
			print query
			if params:
				print params
			print
		try:
			if params:
				cursor.execute(query, params)
			else:
				cursor.execute(query)
		except sqlite.DataError, e:
			raise DataError(str(e))
		except sqlite.OperationalError, e:
			raise OperationalError(str(e))
		except sqlite.IntegrityError, e:
			raise IntegrityError(str(e))
		except sqlite.InternalError, e:
			raise InternalError(str(e))
		except sqlite.ProgrammingError, e:
			raise ProgrammingError(str(e))


	def _get_sqltype(self, fieldtype, fieldname, field):
		if isinstance(field, DbInt) or isinstance(field, DbLong):
			return "INTEGER"
		else:
			super(SqliteAdapter, self)._get_sqltype(
					fieldtype, fieldname, field)
		

	def _create_triggers(self, table):
		""" Used by L{create_table} to create the required 
		foreign key triggers. """
		table_name = table.name
		for fieldname, field in table.iter_fks():
			ftable = field.datasource
			ftable_pk = ftable.get_pk()
			ftable_name = ftable.name
			constraint = "fk_%s" % fieldname
			err = 'on table "%(table_name)s" violates foreign key '\
					'constraint on the "%(fieldname)s" field' % vars()

			qry = \
"""CREATE TRIGGER fk%(act)s_%(table_name)s_%(fieldname)s
BEFORE %(ACT)s ON %(table_name)s
FOR EACH ROW BEGIN
	SELECT RAISE(
		FAIL,
		'update %(err)s'
	) WHERE NEW.%(fieldname)s IS NOT NULL AND
		(SELECT %(ftable_pk)s FROM %(ftable_name)s WHERE %(ftable_pk)s = NEW.%(fieldname)s) IS NULL;
END;
"""
			act = "ins"
			ACT = "INSERT"
			self.execute(qry % vars())

			act = "upd"
			ACT = "UPDATE"
			self.execute(qry % vars())

			qry = \
"""CREATE TRIGGER fkdel_%(table_name)s_%(fieldname)s
BEFORE DELETE ON %(ftable_name)s
FOR EACH ROW BEGIN
	SELECT RAISE(
		FAIL,
		'delete on table "%(ftable_name)s" violates foreign key constraint on the "%(table_name)s.%(fieldname)s" field'
	) WHERE (SELECT %(fieldname)s FROM %(table_name)s WHERE %(fieldname)s = OLD.%(ftable_pk)s) IS NOT NULL;
END;
""" % vars()
			self.execute(qry)


	def _get_sqlfield(self, fieldtype, fieldname, field):
		if fieldtype == Table.FT_FK:
			if field.required:
				n = " NOT NULL"
			else:
				n = ""
			ftable = field.datasource
			c = "CONSTRAINT fk_%s REFERENCES %s(%s) ON DELETE CASCADE" % (
					fieldname, ftable.name, ftable.get_pk())
			d = self._get_sqltype(Table.FT_NORM, fieldname,
					field.datasource.field)
			return "%s %s%s\n\t\t%s" % (fieldname, d, n, c)
		return super(SqliteAdapter, self)._get_sqlfield(
				fieldtype, fieldname, field)

	def create_table(self, table):
		cols = []
		for t, fieldname, field in table.iter_fields():
			if t == Table.FT_MM:
				continue
			d = self._get_sqlfield(t, fieldname, field)
			cols.append(d)
		cols.append("PRIMARY KEY(%s)" % ",".join(table.primary_keys))
		if table.unique:
			cols.append("UNIQUE(%s)" % ",".join(table.unique))
		qry = "CREATE TABLE %s (\n\t%s\n);" % (table.name, ",\n\t".join(cols))
		self.execute(qry)
		self._create_triggers(table)
		self._create_indexes(table)


	def from_db(self, field, value):

		# use the base classes to ensure that subclasses cannot
		# with different from_unicode cannot fuck things up
		if isinstance(field, Date):
			return Date.from_unicode(field, value)
		if isinstance(field, DateTime):
			return DateTime.from_unicode(field, value)
		if isinstance(field, Time):
			return Time.from_unicode(field, value)

		return value
