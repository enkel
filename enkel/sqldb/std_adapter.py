# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from cStringIO import StringIO

from enkel.model.field.base import *
from enkel.model.ds import *

from table import *
from adapter import Adapter
from query import WhereClause, And
from manip_fetcher import *
from dbfields import *


class StdAdapter(Adapter):
	def _get_fk_value(self, datasrc, value):
		""" Get the value of a foreign-key field. Works both
		if the value is a Manip and if it is a normal value.
		"""
		if isinstance(value, Manip):
			pk = datasrc.primary_keys[0]
			return getattr(value, pk)
		else:
			return value


	def _get_sqltype(self, fieldtype, fieldname, field):
		if isinstance(field, String):
			return "VARCHAR(%d)" % field.maxlength
		elif isinstance(field, DbSmallInt):
			return "SMALLINT"
		elif isinstance(field, DbLong):
			return "BIGINT"
		elif isinstance(field, DbInt):
			return "INTEGER"
		elif isinstance(field, Text):
			return "TEXT"
		elif isinstance(field, Date):
			return "DATE"
		elif isinstance(field, Time):
			return "TIME"
		elif isinstance(field, DateTime):
			return "DATETIME"
		elif fieldtype == Table.FT_FK:
			return self._get_sqltype(Table.FT_NORM, fieldname,
					field.datasource.field)
		else:
			raise ValueError("unsupported field-type: %r" % field)

	def _get_sqlfield(self, fieldtype, fieldname, field):
		if field.required:
			n = " NOT NULL"
		else:
			n = ""
		return "%s %s%s" % (fieldname,
				self._get_sqltype(fieldtype, fieldname, field), n)


	def _manip_to_args(self, table, manip, ignore):
		""" Parse a manip for the information needed by
		L{insert} and L{update}.
		
		@return: (fieldnames, params, mm_dict). Where fieldnames is a list
				of fieldnames of normal and foreign-key fields. params is
				a list of the values for normal and foreign-key fields. And
				mm_dict is a dict of values for many-to-many fields.
		"""
		ignore = set(ignore)
		params = []
		fieldnames = []
		mm = {}
		for fieldname, value in manip.iteritems():
			if fieldname in ignore:
				continue
			field = table.model[fieldname]
			if isinstance(field, DatasourceField):
				if isinstance(field, One):
					v = self._get_fk_value(
							field.datasource, value)
					params.append(v)
					fieldnames.append(fieldname)
				elif isinstance(field, Many):
					mmtable = table.mmtables[fieldname]
					m = []
					pk = getattr(manip, table.primary_keys[0])
					for val in value:
						pk2 = self._get_fk_value(field.datasource, val)
						m.append(mmtable.manip(source=pk, target=pk2))
					mm[fieldname] = m
			else:
				params.append(value)
				fieldnames.append(fieldname)
		return fieldnames, params, mm


	def _create_indexes(self, table):
		for index in table.indexes:
			if isinstance(index, UniqueIndex):
				u = " UNIQUE "
			else:
				u = " "
			q = "CREATE%sINDEX %s ON %s(%s)" % (u,
					index.get_name(table.name), table.name,
					",".join(index.fields))
			self.execute(q)




	def paramgen(self, index):
		""" Parameter generator following the interface specified
		L{here <query.paramgen_interface>}.

		Must be implemented in subclasses.
		"""
		raise NotImplementedError()


	def drop_table(self, table):
		self.commit()
		self.cursor.execute("DROP TABLE %s" % table.name)
		self.commit()


	def insert(self, table, manip, ignore=[]):
		fieldnames, params, mm = self._manip_to_args(
				table, manip, ignore)
		values = [self.paramgen(i) for i, v in enumerate(params)]

		qry = "INSERT INTO %s(%s) VALUES(%s)" % (table.name,
				",".join(fieldnames), ",".join(values))
		self.execute(qry, params)

		# insert into many-to-many tables
		for fieldname, manips in mm.iteritems():
			mmtable = table.mmtables[fieldname]
			for m in manips:
				mmtable.insert(m)


	def _manipwhere(self, table, manip):
		a = [getattr(table.c, pk) == manip[pk] for pk in table.primary_keys]
		q, params = And(*a).compile(None, self.paramgen)
		return q, params

	def delete(self, table, manip):
		f, p, mm = self._manip_to_args(table, manip, [])

		if mm:
			pk = manip[table.get_pk()]
			for fieldname in mm:
				mmtable = table.mmtables[fieldname]
				mmtable.qry_delete(mmtable.c.source == pk)

		q, params = self._manipwhere(table, manip)
		qry = "DELETE FROM %s WHERE %s" % (table.name, q)
		self.execute(qry, params)


	def update(self, table, manip, mm_add=[], ignore=[]):
		ignore.extend(table.primary_keys)
		fieldnames, params, mm = self._manip_to_args(
				table, manip, ignore)
		values = ["%s=%s" % (fn, self.paramgen(i))
				for i, fn in enumerate(fieldnames)]

		q, p = self._manipwhere(table, manip)
		params += p
		qry = "UPDATE %s SET %s WHERE %s" % (table.name, ",".join(values), q)
		self.execute(qry, params)

		if mm:
			# remove all many-to-many columns not in "mm_add"
			pk = manip[table.get_pk()]
			for fieldname, manips in mm.iteritems():
				if not fieldname in mm_add:
					mmtable = table.mmtables[fieldname]
					mmtable.qry_delete(mmtable.c.source == pk)

			# insert into many-to-many tables
			for fieldname, manips in mm.iteritems():
				mmtable = table.mmtables[fieldname]
				for m in manips:
					mmtable.insert(m)


	def _where_clause(self, table, *ops, **named_ops):
		return WhereClause(table.name, self.paramgen,
				*ops, **named_ops).compile()


	def select(self, table, *ops, **named_ops):
		ignore = set(named_ops.pop("ignore", [])) # using set for fast lookup

		q, params = self._where_clause(table, *ops, **named_ops)

		fields = []
		for fieldname, field in table.model.iteritems():
			if fieldname in table.mmtables or fieldname in ignore:
				continue
			fields.append(fieldname)

		query = "SELECT %s FROM %s %s" % (",".join(fields), table.name, q)
		c = self.new_cursor()
		self.execute(query, params, c)
		return SimpleManipFetcher(c, table)

	def select_one(self, table, *ops, **named_ops):
		named_ops["limit"] = 1
		r = self.select(table, *ops, **named_ops)
		return r.fetchone()




	def _field_alias(self, fieldname, table, sep):
		return "%s.%s AS %s%s%s" % (table.name, fieldname,
				table.name, sep, fieldname)

	def jselect(self, table, *ops, **named_ops):
		ignore = set(named_ops.pop("ignore", [])) # using set for fast lookup

		if not table.foreign_keys:
			return self.select(table, *ops, **named_ops)

		sep = named_ops.get("sep", "_")
		q, params = self._where_clause(table, *ops, **named_ops)

		fields = []
		joins = StringIO()
		for t, fieldname, field in table.iter_fields():
			if t == Table.FT_MM or fieldname in ignore:
				continue
			if t == Table.FT_FK:
				ftable = field.datasource
				for fn in ftable.model:
					if not (fn in ftable.mmtables or field in ignore):
						fields.append(self._field_alias(fn, ftable, sep))
				joins.write("\n\tLEFT OUTER JOIN %s ON %s.%s = %s.%s" % (
						ftable.name,
						table.name, fieldname,
						ftable.name, ftable.primary_keys[0]))
			else:
				fields.append(self._field_alias(fieldname, table, sep))

		query = "SELECT %s\nFROM %s%s\n%s" % (
				",\n\t".join(fields), table.name, joins.getvalue(), q)
		c = self.new_cursor()
		self.execute(query, params, c)
		return JoinManipFetcher(c, table)

	def jselect_one(self, table, *ops, **named_ops):
		named_ops["limit"] = 1
		r = self.jselect(table, *ops, **named_ops)
		return r.fetchone()




	def rselect(self, table, *ops, **named_ops):
		ignore = set(named_ops.pop("ignore", [])) # using set for fast lookup

		q, params = self._where_clause(table, *ops, **named_ops)

		fields = []
		for fieldname, field in table.model.iteritems():
			if fieldname in table.mmtables or fieldname in ignore:
				continue
			fields.append(fieldname)

		query = "SELECT %s FROM %s %s" % (",".join(fields), table.name, q)
		c = self.new_cursor()
		self.execute(query, params, c)
		return RecursiveManipFetcher(c, table)

	def rselect_one(self, table, *ops, **named_ops):
		named_ops["limit"] = 1
		r = self.rselect(table, *ops, **named_ops)
		return r.fetchone()
