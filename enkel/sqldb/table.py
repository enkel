# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from enkel.model.ds import One, Many, Datasource
from enkel.model.data import Manip

from query import QueryField
from errors import TableStructureError
from dbfields import AutoincPk


class QueryFieldContainer(object):
	def __init__(self, **query_fields):
		"""
		@param query_fields: Keys are fieldnames and values are
				L{query.QueryField}s. The values/QueryFields are
				made part of the objects namespace as obj.<fieldname>.
		"""
		self.__dict__.update(query_fields)


class Index(object):
	""" Defines a table index.

	A index makes operations involving the indexed fields
	much faster in many cases.
	"""
	def __init__(self, *fields):
		"""
		@param fields: one or more L{query.QueryField}s
		"""
		self.fields = fields
	def get_name(self, table_name):
		""" Get the name of the index.
		The name is created by combining the name of the table and
		the name of all fields in the index. """
		return "ix_%s_%s" % (table_name, "_".join(self.fields))

class UniqueIndex(Index):
	""" Defines a unique table index.

	A unique index is the same as an L{Index} but it also makes
	sure all rows in the table contains a unique combination
	of values for the fields in the index.
	"""


class Table(Datasource):
	""" Database table.

	Usage (using sqlite)
	====================
		>>> from enkel.model import ds
		>>> from enkel.model.field.base import String
		>>> from sqlite_adapter import SqliteAdapter
		>>> from query import Asc, Desc


		Get a database connection
		-------------------------

		>>> adapter = SqliteAdapter(":memory:")


		Create tables
		-------------

		>>> artisttable = Table("artists")
		>>> artistmodel = dict(
		... 	name = String(),
		... 	favourite_artist = ds.One(artisttable, required=False)
		... )
		>>> artisttable.set_model(artistmodel, ["name"])

		>>> bandtable = Table("bands")
		>>> bandmodel = dict(
		... 	name = String(),
		... 	lead_singer = ds.One(artisttable),
		... 	members = ds.Many(artisttable)
		... )
		>>> bandtable.set_model(bandmodel, ["name"])

		>>> adapter.register(artisttable, bandtable)
		>>> for t in artisttable, bandtable:
		... 	t.create()


		Insert data
		-----------
		>>> jonathan = artisttable.manip(name="Jonathan")
		>>> reginald = artisttable.manip(name="Reginald",
		... 		favourite_artist="Jonathan")
		>>> for a in jonathan, reginald:
		... 	artisttable.insert(a)

		>>> korn = bandtable.manip(name="Korn",
		... 	lead_singer = jonathan,
		... 	members = (jonathan, "Reginald")
		... )
		>>> bandtable.insert(korn)

		>>> adapter.commit()


		Simple select
		-------------

		>>> for artist in artisttable.select(
		...			order_by=[Asc(artisttable.c.name)]):
		... 	print artist.name, "::", artist.favourite_artist
		Jonathan :: None
		Reginald :: Jonathan

		>>> korn = bandtable.rselect_one(name="Korn")
		>>> print korn.lead_singer.name
		Jonathan


		Advanced select
		---------------

		>>> [m for m in
		... 	korn.members.iter_simple(order_by=[Asc("target")])]
		[u'Jonathan', u'Reginald']

		>>> [member.name for member in
		... 	korn.members.iter_join(order_by=[Desc(artisttable.c.name)])]
		[u'Reginald', u'Jonathan']


	Thread-safety
	=============
		Table instances are thread-safe. But remember that you
		cannot change any of the table-objects variables, since
		they will be changed in all threads. This means that
		L{set_adapter}, L{add_index}, L{ds_iter_setup} cannot
		be used on objects shared by multiple threads.


	@cvar FT_NORM: used by L{iter_fields} to describe a non-foreign-key
			and non-many-to-many field.
	@cvar FT_FK: used by L{iter_fields} to describe a foreign-key field.
	@cvar FT_MM: used by L{iter_fields} to describe a many-to-many field.

	@ivar name: The name of the table.
	@ivar model: The model for the table.
	@ivar adapter: The adapter in use.
	@ivar mmtables: All many-to-many tables assosiated with this table
			as a dict with fieldname's as keys and L{Table}'s as values.
	@ivar primary_keys: List of primary-key fieldnames.
	@ivar foreign_keys: List of foreign-key fieldnames.
	@ivar unique: List of fieldnames for unique fields.
	@ivar indexes: List of all L{Index} objects added with L{add_index}.
	@ivar field: alias for self.model[primary_keys[0]]. Part of
			the L{enkel.model.ds.Datasource} implementation.
	@ivar c: A L{QueryFieldContainer} object containing
			L{query.QueryField}s for all fields in the model.

	@see: L{define_mm_table} for a definition of many-to-many
			tables.


	@group Table definition: __init__, set_model, add_index
	@group Administer table: create, drop
	@group Edit data: insert, update, delete, qry_delete
	@group Query data: select, select_one, jselect, jselect_one,
			rselect, rselect_one
	@group Datasource interface implementation: ds_validate,
			ds_iter_setup, ds_iter, ds_iter_strings
	"""
	FT_NORM, FT_FK, FT_MM, FT_PK = 1, 2, 3, 4
	def __init__(self, name, mmformat="%(source)s_mm_%(target)s"):
		"""
		@param name: The name of the table.
		@param mmformat: A string format used to generate many-to-many
				table names. source and target keywords are replaced
				by the name of this table and the target table.
		"""
		self.adapter = None
		self.mmformat = mmformat
		self.name = name


	def set_model(self, model, primary_keys, unique=[]):
		""" Set the datamodel for the table.

		The model
		=========
			What we mean with a "model" is defined L{here <enkel.model>}.
			Table-models have some additional resctrictions.
			They can only contain fields which are instances of
			these field types:
				- L{dbfields.DbSmallInt}
				- L{dbfields.DbInt}
				- L{dbfields.DbLong}
				- L{dbfields.AutoincPk}
				- L{enkel.model.field.base.String}
				- L{enkel.model.field.base.Text}
				- L{enkel.model.field.base.Time}
				- L{enkel.model.field.base.Date}
				- L{enkel.model.field.base.DateTime}
			Additional restrictions might be defined in the documentation
			for the field.

		@param model: The model defining the table as described above.
		@param primary_keys: A list of primary-key-field names.
		@param unique: A list of fields. A constraint making sure
				that the fields only can contain unique values will
				be create for each field.
		@note: This method is separated from __init__ to enable the
				definition of recursive relations.
		"""
		if len(primary_keys) < 1:
			raise TableStructureError(
				"%s: Tables must have at least one primary-key" % name)
		self.model = model
		self.primary_keys = primary_keys
		self.unique = unique

		self.mmtables = {}
		self.foreign_keys = []
		self.indexes = []
		self.field = self.model[primary_keys[0]] # Datasource interface

		# define QueryField's, foreign-keys and mmtables
		qf = {}
		for t, fieldname, field in self.iter_fields():
			if isinstance(field, Many):
				if isinstance(field.datasource, Table):
					self.mmtables[fieldname] = self.define_mm_table(
							field.datasource)
				else:
					raise TableStructureError(
"""%s::%s: Many() fields can only have Table's as datasource when
used in database-table models. """ % self.name, fieldname)

			elif isinstance(field, AutoincPk):
				if t != self.FT_PK:
					raise TableStructureError(
						"%s::%s: a AutoincPk() field must be the "\
						"primary-key of the table. " % (self.name, fieldname))
				elif len(primary_keys) != 1:
					raise TableStructureError(
						"%s::%s: tables with a AutoincPk() field can "\
						"only have one primary-key" % (self.name, fieldname))

			else:
				if t == self.FT_FK:
					self.foreign_keys.append(fieldname)
				qf[fieldname] = QueryField(fieldname, self.name)
		self.c = QueryFieldContainer(**qf)

		# datasource defaults
		self.ds_iter_labelf = "%%(%s)s" % self.primary_keys[0]
		self.ds_iter_ops = []
		self.ds_iter_named_ops = {}


	def add_index(self, index):
		""" Add index to the table.

		Indexes are created when L{create} is invoked, and removed
		when L{drop} is invoked.

		@param index: A L{Index} or L{UniqueIndex} object.
		"""
		self.indexes.append(index)


	def set_adapter(self, adapter):
		""" Set the adapter used for operations on the table.
		@param adapter: A L{adapter.Adapter} object.
		"""
		self.adapter = adapter
		for fieldname, mmtable in self.mmtables.iteritems():
			mmtable.adapter = adapter


	def manip(self, **values):
		""" Get a L{enkel.model.data.Manip} for the table model.
		@param values: Initial values (just like in Manip).
		"""
		return Manip(self.model, **values)


	def define_mm_table(self, ftable):
		""" Define a many-to-many table.

		Used internally (called by __init__), but globally visible
		for documentation.

		A many-to-many table?
		=====================
			A many-to-many table is a table with only two columns.
			One referencing a row in this table, and one referencing
			a row in another table.

			many-to-many tables are created and handled automatically
			by this library. But for those who wish to use them, they
			are named after the two referenced tables. So a
			L{enkel.model.ds.Many} field from a table named
			"a" to a table named "b", would create a many-to-many-table
			named a_mm_b. The name-format can be overridden by using the
			mmformat parameter to L{__init__}.

		@param ftable: The other table in the many-to-many relation.
		@type ftable: L{Table}.
		@return: A L{Table} defining the many-to-many relationship.
		"""
		a = len(self.primary_keys)
		b = len(ftable.primary_keys)
		if not (a == 1 and b == 1):
			raise ValueError("%s: "\
					"Cannot create many-to-many relations between tables "
					"with more than one primary key" % fieldname)

		mm_model = dict(
			source = One(self),
			target = One(ftable)
		)

		name = self.mmformat % dict(source=self.name,
				target=ftable.name)
		mmtable = Table(name, mmformat=self.mmformat)
		mmtable.set_model(mm_model, ["source", "target"])
		return mmtable

	def iter_fks(self):
		""" Iterate over all foreign-key fields in the table yielding
		(fieldname, field) pairs.
		"""
		for fieldname in self.foreign_keys:
			field = self.model[fieldname]
			yield fieldname, field

	def iter_pks(self):
		""" Iterate over all primary-key fields in the table yielding
		(fieldname, field) pairs.
		"""
		for fieldname in self.primary_keys:
			field = self.model[fieldname]
			yield fieldname, field

	def iter_fields(self):
		""" Iterate over all fields in the table model returning
		(type, fieldname, field). Where type is one of L{FT_FK}, L{FT_MM}
		and L{FT_NORM}. fieldname and field means the same as they do
		in the table model. """
		for fieldname, field in self.model.iteritems():
			if isinstance(field, One) and isinstance(field.datasource, Table):
				t = self.FT_FK
			elif isinstance(field, Many) and isinstance(field.datasource, Table):
				t = self.FT_MM
			elif fieldname in self.primary_keys:
				t = self.FT_PK
			else:
				t = self.FT_NORM
			yield (t, fieldname, field)


	def get_pk_queryfield(self):
		""" Get the queryfield for the primary key in this table.

		This is a convenient shortcut when working with relations.
		@raise RuntimeError: If there is not exactly 1 primary key
				in the table.
		"""
		if len(self.primary_keys) == 1:
			return getattr(self.c, self.primary_keys[0])
		else:
			raise RuntimeError("get_pk_queryfield() only works if there are "\
					"exactly one primary key in the table")

	def get_pk(self):
		""" Get the first primary key.
		This is a convenient shortcut when working with relations.
		@raise RuntimeError: If there is not exactly 1 primary key
				in the table.
		"""
		if len(self.primary_keys) == 1:
			return self.primary_keys[0]
		else:
			raise RuntimeError("get_pk() only works if there are "\
					"exactly one primary key in the table")


	def create(self):
		""" Create table and all many-to-many tables.

		Does not create the tables refereced by the many-to-many
		reference, only the tables created to contain the
		relationships between this table and the other.

		@see: L{define_mm_table} for a definition of many-to-many
				tables.
		"""
		self.adapter.create_table(self)
		for fieldname, table in self.mmtables.iteritems():
			table.create()

	def drop(self):
		""" Drop table and all many-to-many tables created
		by L{create}. """
		for fieldname, table in self.mmtables.iteritems():
			table.drop()
		self.adapter.drop_table(self)


	def insert(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.insert}. """
		self.adapter.insert(self, *args, **kw)
	def update(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.update}. """
		self.adapter.update(self, *args, **kw)

	def delete(self, manip):
		""" Wrapper around L{adapter.Adapter.delete}. """
		self.adapter.delete(self, manip)
	def qry_delete(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.qry_delete}. """
		self.adapter.qry_delete(self, *args, **kw)

	def select(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.select}. """
		return self.adapter.select(self, *args, **kw)
	def select_one(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.select_one}. """
		return self.adapter.select_one(self, *args, **kw)

	def jselect(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.jselect}. """
		return self.adapter.jselect(self, *args, **kw)
	def jselect_one(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.jselect_one}. """
		return self.adapter.jselect_one(self, *args, **kw)

	def rselect(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.rselect}. """
		return self.adapter.rselect(self, *args, **kw)
	def rselect_one(self, *args, **kw):
		""" Wrapper around L{adapter.Adapter.rselect_one}. """
		return self.adapter.rselect_one(self, *args, **kw)



	def ds_validate(self, fieldname, value):
		"""
		@raise FieldValidationError:
				If L{field}.validate(fieldname, value) fails.
		"""
		if isinstance(value, Manip):
			value = value[self.get_pk()]
		self.field.validate(fieldname, value)


	def ds_iter_setup(self, labelf, *ops, **named_ops):
		""" Setup how L{ds_iter} behaves.

		I{ops} and I{named_ops} means the same as they do in
		L{query.WhereClause.__init__}. They are forwarded to L{select}
		by L{ds_iter}.

		@param labelf: A format-string used to generate the
				label in L{ds_iter}. labelf is applied to each Manip
				retured from the select() in ds_iter.
		"""
		self.ds_iter_labelf = labelf
		self.ds_iter_ops = ops
		self.ds_iter_named_ops = named_ops

	def ds_iter(self):
		""" Iterate over all rows in the table using L{select} yielding
		(value, label) pairs.

		The format of the label and arguments to L{select} can be specified
		using L{ds_iter_setup}.
		"""
		pk = self.get_pk()
		ignore = self.model.keys()
		for row in self.select(*self.ds_iter_ops, **self.ds_iter_named_ops):
			yield row[pk], self.ds_iter_labelf % row


def suite():
	import doctest
	return doctest.DocTestSuite()

if __name__ == "__main__":
	from enkel.wansgli.testhelpers import run_suite
	run_suite(suite())
