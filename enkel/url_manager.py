# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

""" Simplifies url recreation. """


class UrlManager(object):
	""" Simplifies url recreation.
	
	Examples
	========
		>>> env = {
		... 	'wsgi.url_scheme': "http",
		... 	'SERVER_NAME': "example.com",
		... 	'SERVER_PORT': "80",
		... 	'SCRIPT_NAME': "/home/share",
		... 	'PATH_INFO': "/test.cgi"
		... }
		>>> u = UrlManager(env)
		>>> u.http_host
		'example.com'
		>>> u.path
		['home', 'share', 'test.cgi']
		>>> u.create()
		'http://example.com/home/share/test.cgi'

		>>> env["SERVER_PORT"] = "8000"
		>>> UrlManager(env).http_host
		'example.com:8000'

		>>> env["HTTP_HOST"] = "www.example.com:90"
		>>> u = UrlManager(env)
		>>> u.http_host
		'www.example.com:90'
		>>> del u.path[-1]
		>>> u.create()
		'http://www.example.com:90/home/share'

	@ivar scheme: URL scheme like http, https or ftp.
			Defaults to the value in env["wsgi.url_scheme"] or
			an empty string if not in env.
	@ivar http_host: The host name and port like "example.com:3000".
			Defaults to env["HTTP_HOST"]. If HTTP_HOST is not in env,
			this is created from env["SERVER_NAME"] and env["SERVER_PORT"].
	@ivar path: A list where each entry is a part of the full path
			on the server. If we are at
			"http://example.com/my/test/app?test=20", path will be
			["my", "test", "app"].
	@ivar query_string: The part of the URL after ?. Defaults to
			and empty string.
	"""
	def __init__(self, env):
		"""
		@param env: A WSGI environ dict.
		"""
		self.scheme = env.get("wsgi.url_scheme", "")
		self.query_string = ""
		self.http_host = env.get("HTTP_HOST", "")
		if not self.http_host:
			self.http_host = env.get("SERVER_NAME", "")
			port = env.get("SERVER_PORT", "")
			if self.scheme == "http":
				if not port == "80":
					self.http_host += ":" + port
			if self.scheme == "https":
				if not port == "443":
					self.http_host += ":" + port
		self.path = env.get("SCRIPT_NAME", "").split("/")
		self.path.append(env.get("PATH_INFO", "/")[1:])
		if len(self.path) > 0 and self.path[0] == "":
			del self.path[0]

	def create(self):
		""" Generate/recreate the url. """
		url = self.scheme + "://" + self.http_host + "/" + "/".join(self.path)
		if self.query_string:
			url += "?" + self.query_string
		return url

	def __str__(self):
		return self.gen()
