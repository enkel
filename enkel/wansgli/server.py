# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

""" Defines a basic standalone WSGI server/handler.

WSGI is specified in PEP 333 which can be found
U{here <http://www.python.org/dev/peps/pep-0333>}.
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from os import environ
from datetime import datetime
from sys import stderr

from apprunner import run_app, Response
from utils import rfc1123_date
from env import urlpath_to_environ


class ServerResponse(Response):
	""" Adds automatic adding of required http headers
	to the L{apprunner.Response} class. Headers are only added
	when not supplied by the app. These headers are handled:
		- server (defaults to L{__init__} parameter I{server_info})
		- date (defaults to the UTC/GMT time when the response is sent)
	"""
	def __init__(self, server_info, *args, **kw):
		super(ServerResponse, self).__init__(*args, **kw)
		self.server_info = server_info

	def validate_header(self, name, value):
		if name in ("server", "date"):
			try:
				del self.extra_headers[name]
			except KeyError:
				pass

	def generate_headers(self):
		self.extra_headers["server"] = self.server_info
		self.extra_headers["date"] = rfc1123_date(datetime.utcnow())
		return super(ServerResponse, self).generate_headers()



class WsgiRequestHandler(BaseHTTPRequestHandler):
	""" A WSGI request handler. You do not call this directly,
	but send it as a parameter to L{WsgiServer.__init__}.

	@cvar ENV: Default values for the WSGI environ dict. See
			L{create_env} for more information.
	"""

	ENV = {}

	def do_GET(self):
		self.handle_wsgi_request("GET")
	def do_POST(self):
		self.handle_wsgi_request("POST")
	def do_OPTIONS(self):
		self.handle_wsgi_request("OPTIONS")
	def do_HEAD(self):
		self.handle_wsgi_request("HEAD")
	def do_PUT(self):
		self.handle_wsgi_request("PUT")
	def do_DELETE(self):
		self.handle_wsgi_request("DELETE")
	def do_TRACE(self):
		self.handle_wsgi_request("TRACE")
	def do_CONNECT(self):
		self.handle_wsgi_request("CONNECT")


	def create_env(self, method):
		""" Create the WSGI environ dict.

		These variables are defined:
			- byte strings:
				- REQUEST_METHOD
				- SERVER_PROTOCOL
				- SERVER_NAME
				- SERVER_PORT
				- CONTENT_TYPE
				- CONTENT_LENGTH
				- REMOTE_ADDR
				- wsgi.url_scheme
			- wsgi.version 
			- wsgi.input       (file-like object)
			- wsgi.errors      (file-like object)
			- wsgi.multithread (bool)
			- wsgi.run_once    (bool)
		And all HTTP-headers provided by the client prefixed with
		'HTTP_'.

		@note: This is the most minimal environment allowed by
		PEP 333. You might wish to subclass this to provide
		more environment variables.

		@return: The WSGI environ dict to be sent to the application.
		"""
		env = self.ENV.copy()

		env.update({
			"REQUEST_METHOD": method,
			"SERVER_PROTOCOL": self.protocol_version,
			"SERVER_NAME": self.server.host,
			"SERVER_PORT": str(self.server.port),
			"CONTENT_TYPE": self.headers.get("content-type", ""),
			"CONTENT_LENGTH": self.headers.get("content-length", ""),
			"REMOTE_ADDR": self.client_address[0],
			"wsgi.version": (1, 0),
			"wsgi.url_scheme": self.server.scheme,
			"wsgi.input": self.rfile,
			"wsgi.errors": self.get_errorfile(),
			"wsgi.multithread": self.server.multithread,
			"wsgi.multiprocess": self.server.multiprocess,
			"wsgi.run_once": self.server.run_once
		})

		# Add all http headers client provided
		for name in self.headers:
			value = self.headers.get(name)
			env["HTTP_" + name.upper()] = value

		return env


	def handle_wsgi_request(self, method):
		""" Create a WSGI environ dict and run the app. """

		# Create the WSGI environ dict
		env = self.create_env(method)

		# parse path
		urlpath_to_environ(env, self.path)

		req = ServerResponse(self.server.server_info, self.wfile, env,
				self.server.debug)
		run_app(self.server.app, req)


	def get_errorfile(self):
		""" Get the file-like object to use as wsgi.errors.
		Defaults to sys.stderr.

		If you want to provide logging, subclass and return another
		file-like object here.
		"""
		return stderr


class WsgiServer(HTTPServer):
	"""
	@cvar run_once: Should evaluate true if the server or gateway expects
			(but does not guarantee!) that the application will only be
			invoked this one time during the life of its containing process.
			Normally, this will only be true for a gateway based on CGI (or
			something similar).
	@cvar multiprocess: This value should evaluate true if an equivalent
			application object may be simultaneously invoked by another
			process, and should evaluate false otherwise.
	@cvar multithread: 	This value should evaluate true if the application
			object may be simultaneously invoked by another thread in the
			same process, and should evaluate false otherwise.
	"""
	run_once = False
	multiprocess = False
	multithread = False

	def __init__(self, app, host="localhost", port=8000,
			handler=WsgiRequestHandler, server_info="a WSGI server",
			debug=False):
		"""
		@param app: A WSGI app as defined in PEP 333.
		@param host: The host to listen on.
		@param port: The port to listen on.
		@param handler: The request-handler used to handle requests.
		@param server_info: A short information string sent to clients
				using the "SERVER" header. Note that this can be
				overridden by the app.
		@param debug: Run in application-debugging mode.
		"""
		self.app = app
		self.scheme = "http"
		self.host = host
		self.port = port
		self.server_info = server_info
		self.debug = True
		HTTPServer.__init__(self, (host, port), handler)
