# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

""" Defines a simple way of working with groups of threads. """


from threading import Thread, Condition


class ThreadPoolThread(Thread):
	""" Used by L{ThreadPool} to spawn threads. """
	def __init__(self, endthread, target, args, kwargs):
		Thread.__init__(self)
		self.endthread = endthread
		self.target = target
		self.args = args
		self.kwargs = kwargs

	def run(self):
		self.target(*self.args, **self.kwargs)
		self.endthread(self)

class ThreadPoolClosedError(Exception):
	""" Raised when L{ThreadPool.add_thread} is invoked after the
	pool has been closed. """

class ThreadPool(Thread):
	"""
	>>> from cStringIO import StringIO
	>>> from threading import Timer

	>>> def test1():
	... 	pass
	>>> def test2(sec, msg, buf):
	... 	t = Timer(sec, buf.write, [msg])
	... 	t.start()
	... 	t.join()
	
	>>> buf = StringIO()
	
	>>> pool = ThreadPool(2)
	>>> pool.add_thread(test2, 0.5, "1", buf)
	>>> pool.add_thread(test1)
	>>> pool.add_thread(test2, 0.3, "2", buf)
	>>> pool.add_thread(test2, 0.1, "3", buf)
	>>> pool.join()
	
	>>> buf.getvalue()
	'231'
	"""
	def __init__(self, maxthreads):
		""" Spawn a new thread and returns immediatly.

		@param maxthreads: Maximum number of threads in the pool.
		"""
		super(ThreadPool, self).__init__()
		self.maxthreads = maxthreads
		self.queue = list()
		self.threads = set()
		self.closed = False
		self.lock = Condition()
		self.start()

	def _send_queue(self):
		for task in self.queue:
			thread = ThreadPoolThread(self._endthread, **task)
			self.threads.add(thread)
			thread.start()
		del self.queue[:]

	def run(self):
		""" Manage the pool. """
		self.lock.acquire()
		while not self.closed:
			self._send_queue()
			self.lock.wait()
		self._send_queue()

		while True:
			if len(self.threads) == 0:
				break
			self.lock.wait()
		self.lock.release()

	def add_thread(self, target, *args, **kwargs):
		""" Add a thread to the pool. """
		if self.closed:
			raise ThreadPoolClosedError(
					"You cannot call add_thread after a call to close() or join().")
		self.lock.acquire()
		while len(self.threads) + len(self.queue) >= self.maxthreads:
			self.lock.wait() # wait for a (delete) notification
		self.queue.append(dict(target=target, args=args, kwargs=kwargs))
		self.lock.notifyAll()
		self.lock.release()

	def close(self):
		""" Block adding of more tasks. """
		self.lock.acquire()
		self.closed = True
		self.lock.notifyAll()
		self.lock.release()

	def join(self):
		""" Wait for the pool to finish. Invokes L{close}.. """
		self.close()
		super(ThreadPool, self).join()

	def _endthread(self, thread):
		""" Remove a thread from the pool.
		Called by L{ThreadPoolThread.run} after target has returned.
		"""
		self.lock.acquire()
		self.threads.remove(thread)
		self.lock.notifyAll()
		self.lock.release()



def _test():
	import doctest
	doctest.testmod()

if __name__ == "__main__":
	_test()
