#!/usr/bin/env python


# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from enkel.batteri.rest_route import RESTroute
from enkel.batteri.staticfiles import StaticFiles
from enkel.batteri.admin.admin import Admin
from enkel.batteri.staticblog.edit import StaticBlogEdit
from enkel.wansgli.server import WsgiServer, WsgiRequestHandler
from enkel.batteri.auth.http_auth import BasicHttpAuth
from enkel.batteri.auth.memory_userdb import UserDb


userdb = UserDb()
userdb.add("enkel", "enkel", "admin")


route = RESTroute()

xslt = StaticFiles("xslt", prefix="/xslt/")
route.add("/xslt/>", GET=xslt)

xslt = StaticFiles("pubblog", prefix="/blog/")
route.add("/blog/>", GET=xslt)

admin = Admin("/xslt/xhtml/admin.xsl")
admin.add("staticblog", StaticBlogEdit, u"Static blog",
		configfile="staticblog.cfg")
admin.compile()
authadmin = BasicHttpAuth(admin, userdb, "admin", "admin panel")
route.add("/admin/>", GET=authadmin, POST=authadmin)

s = WsgiServer(route)
print "Listening on %(host)s:%(port)s" % vars(s)
s.serve_forever()
