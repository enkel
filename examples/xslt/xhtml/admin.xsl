<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:m="http://enkel.sourceforge.net/xml/markup"
		xmlns:a="http://enkel.sourceforge.net/xml/admin"
		xmlns:f="http://enkel.sourceforge.net/xml/form">

	<xsl:import href="modules/structural.xsl"/>
	<xsl:import href="modules/list.xsl"/>
	<xsl:import href="modules/media.xsl"/>
	<xsl:import href="modules/inline.xsl"/>
	<xsl:import href="modules/table.xsl"/>
	<xsl:import href="modules/form.xsl"/>
	<xsl:import href="modules/admin.xsl"/>

	<xsl:output method="html" encoding="utf-8"/>



	<xsl:template match="f:text[@typehint='XmlField']">
		<xsl:variable name="w">
			<textarea id="{@id}" name="{@id}" rows="20" cols="30"
					wrap="off" title="{f:tooltip/text()}"
					class="form_xml">
				<xsl:value-of select="f:value/text()"/>
			</textarea>
		</xsl:variable>
		<xsl:call-template name="f:create-field">
			<xsl:with-param name="input-widget" select="$w"/>
		</xsl:call-template>
	</xsl:template>



	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					<!-- See section.xsl for all the relevant markup
					styles. -->


					#header, #body{
						margin-left: auto;
						margin-right: auto;
						width: 750px;
						padding: 5px 20px 5px 20px;
					}
					#body{
						clear: both;
					}

					#main{
						margin-left: 180px;
					}

					#header{
						font-size: 2.4em;
						font-family: "trebuchet ms";
						border-width: 4px 0 4px 0;
						border-style: solid;
						border-color: #000;
						margin-top: 20px;
						margin-bottom: 20px;
					}

					<!-- structural -->
					div .markup_section{}
					div .markup_p{margin: 10px 0 10px 0;}
					pre .markup_pre
					pre .markup_prog{}
					h1{margin-top: 0;}
					h2{}
					h3{}
					h4{}
					h5{}
					h6{}

					<!-- Inline elements -->
					a{}
					code{}
					strong{}
					em{}

					<!-- <image> -->
					.markup_image{}
					.markup_image p{}
					.markup_image img{}


					<!-- form styles -->
					form .form_{}
					div .form_item{
						padding: 10px 0 10px 0;
						margin-left: 120px;
					}
					div .form_item_required label{
						font-weight: bold;
					}
					div .form_item label{
						display: block;
						float: left;
						width: 100px;
						text-align: left;
						margin-left: -120px;
					}
					div .form_formerror{
						color: red;
					}
					div .form_item input[type="text"]{
						border: 1px solid #aaa;
					}
					div .form_item input[type="text"]:hover{
						border: 1px solid #000;
					}
					div .form_item input[type="text"]:focus{
						border: 1px solid #000;
						background-color: #eee;
					}

					div .form_item textarea{
						border: 1px solid #aaa;
						width: 100%;
					}
					div .form_item textarea:hover{
						border: 1px solid #000;
					}
					div .form_item textarea:focus{
						border: 1px solid #000;
						background-color: #eee;
					}
					div .form_item textarea.form_longstring{}
					div .form_item textarea.form_text{
						white-space: pre;
					}

					div .form_submit{}
					div .form_submit input[type="submit"]{}


					<!-- Table -->
					table{
						border: 1px solid #000;
						border-spacing: 0;
						border-collapse: collapse;
					}
					th, td{
						padding: 5px 10px 5px 10px;
						border-left: 1px solid #000;
						font-size: 1.0em;
					}
					th{
						border-bottom: 3px solid #000;
						border-left: 1px solid #000;
					}
					td{
						border-bottom: 1px solid #000;
					}


					<!-- admin -->
					#admin_applist{
						width: 150px;
						list-style: none;
						margin: 0;
						padding: 0;
						float: left;
					}
					#admin_applist li{
						margin-bottom: 10px;
					}
					#admin_applist li .admin_app_label{
						border-bottom: 2px solid #000;
					}
					#admin_applist ul{
						padding: 0;
						margin: 0 0 0 20px;
					}
					#admin_applist ul li{
						list-style: none;
						margin: 0;
					}
					span .admin_app_label{}
					a .admin_action_label{}
				</style>
			</head>
			<body>
				<div id="header">
					Enkel administration panel
				</div>
				<div id="body">
					<xsl:apply-templates select="a:admin/a:applist"/>
					<div id="main">
						<xsl:apply-templates select="a:admin/m:section"/>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
