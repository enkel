<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:a="http://enkel.sourceforge.net/xml/admin">


	<xsl:template match="a:admin/a:applist/a:app">
		<li>
			<div class="admin_app_label">
				<xsl:value-of select="@label"/>
			</div>
			<ul>
				<xsl:for-each select="a:action">
				<li>
					<a class="admin_action_label">
						<xsl:attribute name="href">
							<xsl:text>../</xsl:text>
							<xsl:value-of select="../@id"/>
							<xsl:text>/</xsl:text>
							<xsl:value-of select="@id"/>
						</xsl:attribute>
						<xsl:value-of select="@label"/>
					</a>
				</li>
				</xsl:for-each>
			</ul>
		</li>
	</xsl:template>

	<xsl:template match="a:admin/a:applist">
		<ul id="admin_applist">
			<xsl:apply-templates select="a:app"/>
		</ul>
	</xsl:template>

</xsl:stylesheet>
