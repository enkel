<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:m="http://enkel.sourceforge.net/xml/markup">


	<xsl:template match="m:image">
		<div class="markup_image">
			<xsl:attribute name="style">
				<xsl:if test="@width">
					<xsl:text>width:</xsl:text>
					<xsl:value-of select="@width"/>
					<xsl:text>px;</xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="@display = 'inline'">
						<xsl:text>display:inline;float:none;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>display:block;clear:both;</xsl:text>
						<xsl:choose>
							<xsl:when test="@float">
								<xsl:text>float:</xsl:text>
								<xsl:value-of select="@float"/>
								<xsl:text>;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>float=none;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<img src="{@href}">
				<xsl:if test="@width">
					<xsl:attribute name="width">
						<xsl:value-of select="@width"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:if test="@height">
					<xsl:attribute name="height">
						<xsl:value-of select="@height"/>
					</xsl:attribute>
				</xsl:if>
			</img>
			<p>
				<xsl:apply-templates/>
			</p>
		</div>
	</xsl:template>

</xsl:stylesheet>
