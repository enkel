<?xml version="1.0"?>

<!-- Example XSLT scheet for a document with the exml markup
element <section> as root element. -->

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:e="http://enkel.sourceforge.net/xml/exml">

	<xsl:import href="modules/structural.xsl"/>
	<xsl:import href="modules/list.xsl"/>
	<xsl:import href="modules/media.xsl"/>
	<xsl:import href="modules/inline.xsl"/>
	<xsl:import href="modules/table.xsl"/>

	<xsl:output method="html" encoding="utf-8"/>

	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					<!-- structural -->
					div .markup_section{}
					div .markup_p{margin: 10px 0 10px 0;}
					pre .markup_pre
					pre .markup_prog{}
					h1{}
					h2{}
					h3{}
					h4{}
					h5{}
					h6{}

					<!-- Table elements -->
					table{}
					tr{}
					th{}
					td{}

					<!-- Inline elements -->
					a{}
					code{}
					strong{}
					em{}

					<!-- <image> -->
					.markup_image{}
					.markup_image p{}
					.markup_image img{}
				</style>
			</head>
			<body>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
