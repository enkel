from setuptools import setup, find_packages


setup(
	name = "Enkel",
	version = "1.0a1",
	author = "Espen Angell Kristiansen",
	author_email = "espeak@users.sourceforge.net",
	url = "http://enkel.sourceforge.net",

	package_data = {
		"": ["*.rng", "*.txt"],
	},

	packages = find_packages(exclude=["testsuite*"]),

	entry_points = {
		"console_scripts": [
			"enkel-staticblog = enkel.batteri.staticblog.cli",
			#"enkel-benchmark-server = enkel.scripts.benchmark_server",
			"enkel-cgi-server = enkel.scripts.cgi_server",
			"enkel-server-response-tester = enkel.scripts.server_response_tester",
			"enkel-wsgi-server = enkel.scripts.wsgi_server"
		]
	},

	test_suite = "testsuite.suite",
)
