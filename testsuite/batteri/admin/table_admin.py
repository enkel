# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase
from lxml.etree import XML

from enkel.wansgli.testhelpers import unit_case_suite, run_suite
from enkel.wansgli.apptester import AppTester
from enkel.sqldb.sqlite_adapter import SqliteAdapter
from enkel.model.field.base import String, Text
from enkel.sqldb import table, dbfields
from enkel.batteri.admin.table_admin import TableAdmin
from enkel.batteri.admin.admin import Admin, TPL_ENV
from enkel.batteri.admin.validate import validate_admin



infotable = table.Table("info")
model = dict(
	id = String(20),
	id2 = dbfields.DbInt(),
	title = String(50),
	intro = String(250),
	text = Text()
)
infotable.set_model(model, ["id", "id2"])

class InfoTableAdmin(TableAdmin):
	BROWSE_FORMAT = "%(title)s"
	TABLE = infotable
	PER_PAGE = 2
	PRETTY_XML = True


class TestTableAdmin(TestCase):
	def setUp(self):
		adapter = SqliteAdapter(":memory:")
		adapter.register(infotable)
		infotable.create()
		for id, id2, title, intro, text in [
				(u"a", 1, u"A test", u"This is 'a' test", u"hmm"),
				(u"b", 2, u"B test", u"This is 'b' test", u"haa"),
				(u"c", 3, u"\u00e5 test", u"This is '\u00e5' test", u"haa")
			]:
			m = infotable.manip(
				id=id, id2=id2, title=title, intro=intro, text=text)
			m.validate()
			infotable.insert(m)

		self.setup = ("enkel.appsettings", ([u"info", u"Info"], {}))
		self.admin = Admin(pretty=True)
		self.admin.add("ta", InfoTableAdmin, "Table admin test")
		self.admin.compile()

	def testCreate(self):
		t = AppTester(self.admin, url="http://example.com/ta/create")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_get()
		self.assert_(r.body_contains('action id="create"'))
		validate_admin(XML(r.body))

	def testEdit(self):
		p = [("id", "b"), ("id2", "2")]
		t = AppTester(self.admin, p, "http://example.com/ta/edit")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_get()
		self.assert_(r.body_contains('action="edit"'))
		validate_admin(XML(r.body))

	def testSaveNew(self):
		p = [
			("id", "d"),
			("id2", "4"),
			("title", "This is a test"),
			("intro", "Welcome to the testing"),
			("text", "hmm")
		]
		t = AppTester(self.admin, p, "http://example.com/ta/savenew")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_post()

		self.assert_(r.body_contains('action="savenew"'))
		self.assert_(not r.body_contains('<form'))
		self.assertEquals(len([x.id for x in infotable.select()]), 4)
		validate_admin(XML(r.body))

	def testSaveError(self):
		t = AppTester(self.admin, [], "http://example.com/ta/savenew")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_post(True)

		self.assert_(r.body_contains('action="savenew"'))
		self.assert_(r.body_contains('<form'))
		validate_admin(XML(r.body))


	def testDelete(self):
		p = [("id", "b"), ("id2", "2"), ("confirmed", "yes")]
		t = AppTester(self.admin, p, "http://example.com/ta/delete")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_post(True)

		self.assert_(r.body_contains('action="delete"'))
		self.assertEquals(len([x.id for x in infotable.select()]), 2)
		validate_admin(XML(r.body))


	def testBrowse(self):
		t = AppTester(self.admin, url="http://example.com/ta/browse")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_get()
		self.assert_(r.body_ncontains("</row>", 2))
		self.assert_(r.body_contains("next"))
		self.assert_(not r.body_contains("previous"))

		t = AppTester(self.admin, url="http://example.com/ta/browse?page=1")
		t.set_env(TPL_ENV, "test.xsl")
		r = t.run_get()
		self.assert_(r.body_ncontains("</row>", 1))
		self.assert_(not r.body_contains("next"))
		self.assert_(r.body_contains("previous"))
		validate_admin(XML(r.body))



def suite():
	return unit_case_suite(TestTableAdmin)

if __name__ == '__main__':
	run_suite(suite())
