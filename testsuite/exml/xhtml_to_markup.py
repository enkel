# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase
from lxml.etree import XML

from enkel.wansgli.testhelpers import unit_case_suite, run_suite
from enkel.exml.xhtml_to_markup import xhtml_to_markup
from enkel.exml.info import XMLNS_MARKUP
from enkel.exml.validate import validate_section


TPL = """<section xmlns="%(XMLNS_MARKUP)s">
<h>heading</h>
%%s
</section>""" % vars()

class TestXhtmlToMarkup(TestCase):
	def test_hX_to_section(self):
		xhtml = """
		<html>
			<h1>Heading</h1>
			<h2>Heading</h2>
			<h2>Heading</h2>
			<h5>Heading</h5>
			<h3>Heading</h3>
		</html>
		"""
		expected = "<section><h>Heading</h><section><h>Heading</h></section>"\
				"<section><h>Heading</h><section><h>Heading</h></section>"\
				"</section><section><h>Heading</h></section></section>"
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, expected)
		validate_section(XML(TPL % r))


	def test_ignore(self):
		xhtml = """
		<html>
			<head>
				this is ignored.
				<script src="dsadsa"/>
			</head>
			<body>
				<form>
					this is also ignored.
				</form>
			</body>
		</html>
		"""
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, "")


	def test_paragraph(self):
		xhtml = """
		<html>
			<body>
				This becomes a paragraph.
				This will be part of the same paragraph as the text above.

				<p>Another paragraph</p>

				And another...

				<h1>a heading</h1>
				and a parahraph
			</body>
		</html>
		"""
		r = xhtml_to_markup(xhtml, pretty=True)
		self.assert_(r.count("<p>"), 4)
		validate_section(XML(TPL % r))


	def test_a(self):
		xhtml = """
		<body>
			<a href='xx.html' style='color:red'>xx</a>
			<a>aa</a>
		</body>"""
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r.count("<a"), 2)
		self.assertEquals(r.count("xx.html"), 1)
		validate_section(XML(TPL % r))


	def test_inline(self):
		xhtml = """
		<body>
			text
			<b>bold</b><strong>strong</strong>
			<em>em</em><i>italic</i>
			<code>code</code>
		</body>"""
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r.count("<strong>"), 2)
		self.assertEquals(r.count("<em>"), 2)
		self.assertEquals(r.count("<code>"), 1)
		validate_section(XML(TPL % r))

		markup = "<p><strong>bold</strong> text</p><p>more text</p>"
		xhtml = "<body>%s</body>" % markup
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, markup)
		validate_section(XML(TPL % r))

	def test_nested_inline(self):
		xhtml = "<body><b><i><a>test</a></i></b></body>"
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, "<p><strong>test</strong></p>")
		validate_section(XML(TPL % r))


	def test_lists(self):
		xhtml = """
		<body>
			<ul>
				<li>one</li>
				<li>two</li>
			</ul>
		</body>"""
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r.count("<ul>"), 1)
		self.assertEquals(r.count("<li>"), 2)
		self.assertEquals(r.count("one"), 1)
		validate_section(XML(TPL % r))

		xhtml = """
		<body>
			<ol>
				<li>one</li>
				<li>item <b>two</b></li>
			</ol>
			<li>this is ignored</li>
		</body>"""
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r.count("item <strong>two</strong>"), 1)
		self.assertEquals(r.count("ignored"), 0)
		validate_section(XML(TPL % r))


	def test_table(self):
		xhtml = """
		<body>
			<table border="10">
				<tr>
					<td>e1.1</td>
					<th>e1.2</th>
				</tr>
				<tr>
					<td colspan="2" rowspan="10">e2</td>
				</tr>
				<td>this is ignored</td>
			</table>
			<th>this is ignored</th>
			<td>this too</td>
			<tr>and this;)</tr>
		</body>"""
		expected = '<p><table>' \
				'<row><entry>e1.1</entry><entry>e1.2</entry></row>' \
				'<row><entry colspan="2" rowspan="10">e2</entry></row>' \
			'</table></p>'
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, expected)
		validate_section(XML(TPL % r))


	def test_ignore_block_in_inline(self):
		xhtml = """
		<body>
			<p><p>text</p></p>
			<table><tr><td><p>text</p></td></tr></table>
			<ul><li><p>text</p></li></ul>
		</body>
		"""
		expected = "<p>text</p>" \
				"<p><table><row><entry>text</entry></row></table></p>" \
				"<p><ul><li>text</li></ul></p>"
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, expected)
		validate_section(XML(TPL % r))

	def test_image(self):
		xhtml = """
		<body>
			<img src="x.png"/>
			<p>a image: <img src="y.jpg"/></p>
		</body>
		"""
		expected = '<p><image href="x.png"/></p>'\
				'<p>a image: <image href="y.jpg"/></p>'
		r = xhtml_to_markup(xhtml)
		self.assertEquals(r, expected)
		validate_section(XML(TPL % r))


def suite():
	return unit_case_suite(TestXhtmlToMarkup)

if __name__ == '__main__':
	run_suite(suite())
