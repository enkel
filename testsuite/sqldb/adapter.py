# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase

from enkel.sqldb import dbfields
from enkel.sqldb.table import Table, Index, UniqueIndex
from enkel.sqldb.query import Asc, Desc, Or
from enkel.sqldb.errors import *
from enkel.model.field.base import *
from enkel.model.ds import One, Many
from enkel.model.data import Manip


class TestAdapter(TestCase):
	def setUp(self):
		self.tables = [] # list of tables to drop in tearDown().

	def tearDown(self):
		self.tables.reverse()
		for table in self.tables:
			table.drop()


	def create_related_tables(self):
		""" creates some related tables used in many of the tests. """
		food_model = dict(
			name = String(),
			rating = dbfields.DbInt(),
			price = dbfields.DbInt(),
			origin = String(40)
		)
		self.food_table = Table("food")
		self.food_table.set_model(food_model, ["name"], ["origin"])
		self.food_table.add_index(UniqueIndex("rating", "price"))
		self.food_table.add_index(Index("rating"))

		artist_model = dict(
			name = String(),
			rating = dbfields.DbInt(),
			favourite_dish = One(self.food_table),
			hated_food = One(self.food_table)
		)
		self.artist_table = Table("artist")
		self.artist_table.set_model(artist_model, ["name"])

		band_model = dict(
			name = String(50),
			rating = dbfields.DbInt(required=False),
			members = Many(self.artist_table),
			lead_singer = One(self.artist_table),
			food = One(self.food_table)
		)
		self.band_table = Table("band")
		self.band_table.set_model(band_model, ["name"])

		self.adapter.register(self.food_table, self.artist_table,
				self.band_table)

		for table in self.food_table, self.artist_table, self.band_table:
			self.tables.append(table)
			table.create()

	def create_and_insert_related_tables(self):
		""" Run L{create_related_tables} and insert some data
		into the created tables. """
		self.create_related_tables()

		hamburger = self.food_table.manip(name=u"Hamburger",
				rating=6, origin=u"USA", price=4)
		sushi = self.food_table.manip(name=u"Sushi",
				rating=10, origin=u"Japan", price=6)
		steak = self.food_table.manip(name=u"Steak",
				rating=8, origin=u"No idea", price=8)
		for m in hamburger, sushi, steak:
			m.validate()
			self.food_table.insert(m)

		jonathan = self.artist_table.manip(name=u"Jonathan",
					rating=10, favourite_dish=sushi, hated_food=hamburger)
		reginald = self.artist_table.manip(name=u"Reginald",
					rating=9, favourite_dish=hamburger, hated_food=sushi)
		amy = self.artist_table.manip(name=u"Amy Wong",
					rating=5, favourite_dish=sushi, hated_food=steak)
		bender = self.artist_table.manip(name=u"Bender",
					rating=6, favourite_dish=steak, hated_food=sushi)
		james = self.artist_table.manip(name=u"James",
					rating=8, favourite_dish=steak, hated_food=sushi)
		lars = self.artist_table.manip(name=u"Lars",
					rating=7, favourite_dish=hamburger, hated_food=sushi)
		for m in jonathan, reginald, amy, bender, james, lars:
			m.validate()
			self.artist_table.insert(m)

		korn = self.band_table.manip(name=u"Korn",
				members = (jonathan, reginald),
				lead_singer = jonathan,
				food = hamburger)
		metallica = self.band_table.manip(name=u"Metallica",
				members = (james, lars),
				lead_singer = james,
				food = steak)
		pehb = self.band_table.manip(name=u"Planet Express Houseband",
				members = (amy, bender),
				lead_singer = bender,
				food = sushi)
		for m in korn, metallica, pehb:
			m.validate()
			self.band_table.insert(m)

		self.adapter.commit()


	def test_select_simple(self):
		self.create_and_insert_related_tables()
		c = self.band_table.c

		r = [row for row in self.band_table.select()]
		self.assertEqual(len(r), 3)

		korn = self.band_table.select_one(c.name=="Korn")
		self.assertEqual(korn.lead_singer, u"Jonathan")
		self.assert_(isinstance(korn.name, unicode))

		# test ignore keyword
		pe = self.band_table.select_one(ignore=["name"],
				order_by=[Asc(c.name)])
	 	self.assertEquals(pe.name, None)

		# Test many-to-many
		members = [x for x in korn.members]
		self.assertEqual(len(members), 2)
		self.assert_(u"Jonathan" in members)


	def test_select_kw(self):
		self.create_and_insert_related_tables()
		a = self.artist_table
		name = a.c.name
		self.assertEquals(u"Amy Wong",
				a.select_one(order_by=[Asc(name)]).name)
		self.assertEquals(2,
				len([x for x in a.select(limit=2)]))

		self.assertEquals(u"James",
				a.select_one(order_by=[Desc(name)], limit=2, offset=3).name)



	def test_jselect(self):
		self.create_and_insert_related_tables()
		r = self.band_table.jselect(self.food_table.c.name != u"Sushi",
				order_by=[Asc(self.band_table.c.name)])
		res = [x for x in r]
		self.assertEquals(len(res), 2)
		self.assertEquals(res[0].name, u"Korn")
		korn = self.band_table.jselect_one(name=u"Korn")
		self.assertEquals(korn.lead_singer.name, u"Jonathan")

		# unicode
		self.assert_(isinstance(korn.lead_singer.name, unicode))
		for m in korn.members:
			self.assert_(isinstance(m, unicode), "%r???" % type(m))

		# test ignore keyword
		b = self.band_table.c
		pe = self.band_table.jselect_one(ignore=["name"],
				order_by=[Asc(b.name)])
	 	self.assertEquals(pe.name, None)

		# Test many-to-many
		order = [Desc(self.artist_table.c.name)]
		members = [x for x in korn.members.iter_join(order_by=order)]
		self.assertEqual(len(members), 2)
		self.assertEqual(members[0].name, u"Reginald")


	def test_jselect_fails(self):
		self.create_and_insert_related_tables()

		# make sure joins that should fail, fails
		self.assertRaises(AdapterError,
				self.artist_table.jselect_one, name=u"Sushi")


	def test_rselect(self):
		self.create_and_insert_related_tables()
		c = self.band_table.c
		r = [row for row in self.band_table.rselect(order_by=[Asc(c.name)])]
		self.assertEqual(r[1].name, u"Metallica")
		self.assertEqual(r[1].lead_singer.name, u"James")
		self.assertEqual(r[1].lead_singer.favourite_dish.name, u"Steak")
		self.assertEqual(r[1].lead_singer.hated_food.name, u"Sushi")
		self.assert_(isinstance(r[1].name, unicode))
		self.assertEqual(len(r), 3)

		r = self.band_table.rselect_one(c.name==u"Korn")
		self.assert_(isinstance(r.lead_singer, Manip))
		self.assertEqual(r.lead_singer.name, u"Jonathan")
		self.assert_(isinstance(r.lead_singer.favourite_dish, Manip))
		self.assertEqual(r.lead_singer.favourite_dish.name, u"Sushi")


	def test_different_select_methods(self):
		self.create_and_insert_related_tables()
		a = self.artist_table
		name = a.c.name

		self.assertEquals(
				len([x for x in a.select(name.like(u"%m%"))]), 2)
		self.assertEquals(u"Bender",
				a.select_one(name.endswith(u"er")).name)
		self.assertEquals(u"Bender",
				a.select_one(name.startswith(u"Be")).name)

		r = a.select(name!=u"Bender")
		self.assertEquals(len([x for x in r]), 5)

		r = a.select(name!=u"Bender", name!=u"Amy",
				Or(name==u"James", name==u"Reginald"))
		self.assertEquals(len([x for x in r]), 2)

		self.assertEquals(len([x for x in a.select(name.isnull())]), 0)
		self.assertEquals(len([x for x in a.select(name.not_null())]), 6)

		r = [x for x in a.select(a.c.rating.between(7, 10))]
		self.assertEquals(len(r), 4)

		r = [x for x in a.select(name.in_list(u"James", u"Jonathan"))]
		self.assertEquals(len(r), 2)


	def test_delete(self):
		self.create_and_insert_related_tables()
		b = self.band_table
		l = len([x for x in b.select()])

		korn = b.select_one(name=u"Korn")
		b.delete(korn)
		self.assertEquals(len([x for x in b.select()]), l-1)

		b.qry_delete(b.c.name.contains(u"Express"))
		self.assertEquals(len([x for x in b.select()]), l-2)

		self.adapter.commit()


	def test_update(self):
		self.create_and_insert_related_tables()
		b = self.band_table

		korn = b.jselect_one(b.c.name == u"Korn")
		for x in xrange(3): # we repeat to ensure old records are removed
			korn.members = u"James", u"Amy Wong", u"Bender"
			korn.validate()
			b.update(korn)
		korn = b.rselect_one(b.c.name == "Korn")
		self.assertEquals(len([x for x in korn.members]), 3)

		korn = b.rselect_one(b.c.name == "Korn")
		korn.members = [u"Reginald"]
		b.update(korn, mm_add=[u"members"])
		korn = b.rselect_one(b.c.name == u"Korn")
		l = [x.name for x in korn.members.iter_join()]
		l.sort()
		self.assertEquals(l, [u"Amy Wong", u"Bender", u"James", u"Reginald"])

		# ignore kw
		steak = self.food_table.manip(name=u"Steak",
				rating=1000, origin=u"xx", price=20)
		self.food_table.update(steak, ignore=["rating", "origin"])
		steak = self.food_table.select_one(name=u"Steak")
		self.assertEquals(steak.rating, 8)
		self.assertEquals(steak.origin, "No idea")
		self.assertEquals(steak.price, 20)

		self.adapter.commit()

	def test_many_to_many(self):
		self.create_and_insert_related_tables()
		b = self.band_table
		korn = b.select_one(b.c.name == "Korn")
		a = len([x for x in korn.members])
		b = len([x for x in korn.members.iter_join()])
		c = len([x for x in korn.members.iter_recursive()])
		self.assert_(a == b == c == 2)

		# order by
		it = korn.members.iter_simple(order_by=[Desc("target")])
		self.assertEquals(it.next(), u"Reginald")
		it = korn.members.iter_join(order_by=[Desc("target")])
		self.assertEquals(it.next().name, u"Reginald")
		it = korn.members.iter_recursive(order_by=[Desc("target")])
		self.assertEquals(it.next().favourite_dish.name, u"Hamburger")

		# limit
		for x in "iter_simple", "iter_recursive", "iter_join":
			l = [x for x in getattr(korn.members, x)(limit=1)]
			self.assertEquals(len(l), 1)

		# offset
		it = korn.members.iter_recursive(order_by=[Desc("target")],
				limit=1, offset=1)
		self.assertEquals(it.next().name, u"Jonathan")


	def test_rollback(self):
		self.create_and_insert_related_tables()

		# insert
		r = [x for x in self.food_table.select()]
		kebab = self.food_table.manip(name="Kebab",
				rating=4, origin="I don't know", price=3)
		self.food_table.insert(kebab)
		r2 = [x for x in self.food_table.select()]
		self.adapter.rollback()
		r3 = [x for x in self.food_table.select()]
		self.assertEquals(len(r), len(r2)-1, len(r3))

		# update
		korn = self.band_table.jselect_one(name=u"Korn")
		korn.members = [u"Bender"]
		self.band_table.update(korn, mm_add=[u"members"])
		self.adapter.rollback()
		korn = self.band_table.jselect_one(name=u"Korn")
		m2 = [name for name in korn.members]
		self.assertEquals(len(m2), 2)

		# delete
		self.band_table.qry_delete(name=u"Korn")
		self.adapter.rollback()
		self.assertEquals("Korn",
			self.band_table.select_one(name=u"Korn").name)


	def test_foreign_key_constraints(self):
		self.create_and_insert_related_tables()

		# Insert
		fry = self.artist_table.manip(name=u"Fry", 
				rating=10, favourite_dish=u"Hamburger",
				hated_food=u"Does not exist")
		self.assertRaises(IntegrityError, self.artist_table.insert, fry)
		self.adapter.rollback()

		# Update
		bender = self.artist_table.select_one(name=u"Bender")
		bender.hated_food = u"Does not exist"
		self.assertRaises(IntegrityError, self.artist_table.update, bender)
		self.adapter.rollback()

		# Delete
		self.assertRaises(IntegrityError, self.food_table.qry_delete)
		self.adapter.rollback()



	def test_unique(self):
		self.create_and_insert_related_tables()
		
		# unique constraint
		apizza = self.food_table.manip(name=u"American pizza",
				rating=6, origin=u"USA", price=7)
		self.assertRaises(IntegrityError, self.food_table.insert, apizza)
		self.adapter.rollback()

		# unique index
		pizza = self.food_table.manip(name=u"Pizza",
				rating=8, origin=u"Italy", price=8)
		self.assertRaises(IntegrityError, self.food_table.insert, pizza)
		self.adapter.rollback()


	def test_autoincrement(self):
		table = Table("mytable")
		model = dict(id=dbfields.AutoincPk(), name=String())
		table.set_model(model, ["id"])
		table.set_adapter(self.adapter)
		table.create()
		self.tables.append(table)

		# test insert
		for name in "Amy", "Bender", "Fry":
			table.insert(table.manip(name=name))
		self.adapter.commit()
		r = [x.id for x in table.select(order_by = [Asc("id")])]
		self.assertEquals(r, [1, 2, 3])

		# test relations
		table2 = Table("my2table")
		model = dict(id=dbfields.AutoincPk(), b=One(table))
		table2.set_model(model, ["id"])
		table2.set_adapter(self.adapter)
		table2.create()
		self.tables.append(table2)



	def test_all_fieldtypes(self):
		m = dict(
			id = dbfields.DbInt(),
			small = dbfields.DbSmallInt(),
			iint = dbfields.DbInt(),
			llong = dbfields.DbLong(),
			string = String(),
			text = Text(),
			ddate = Date(),
			ttime = Time(),
			ddatetime = DateTime()
		)
		table = Table("typetest")
		table.set_model(m, ["id"])
		table.set_adapter(self.adapter)
		table.create()
		self.tables.append(table)

		x = table.manip(
			id = 1,
			small = 10,
			iint = 20,
			llong = 30,
			string = u"HeHe",
			text = u"Haha",
			ddate = datetime.date(1980, 12, 24),
			ttime = datetime.time(6, 3, 2),
			ddatetime = datetime.datetime(1980, 12, 24, 6, 33, 10),
		)
		table.insert(x)

		# set max values
		x = table.manip(
			id = 1,
			small = 2**15 - 1,
			iint = 2**31 - 1,
			llong = 2**63 - 1,
		)
		table.update(x, ignore=["string", "text", "ddate", "ddatetime", "ttime"])


		# query result
		r = table.select_one()
		self.assert_(isinstance(r.small, int))
		self.assert_(isinstance(r.iint, (long, int)))
		self.assert_(isinstance(r.llong, long))
		self.assert_(isinstance(r.string, unicode))
		self.assert_(isinstance(r.text, unicode))
		self.assert_(isinstance(r.ddate, datetime.date))
		self.assert_(isinstance(r.ttime, datetime.time))
		self.assert_(isinstance(r.ddatetime, datetime.datetime))

		self.assertEqual(r.ddate, datetime.date(1980, 12, 24))
		self.assertEqual(r.ttime, datetime.time(6, 3, 2))
		self.assertEqual(r.ddatetime, datetime.datetime(1980, 12, 24, 6, 33, 10))


	def test_ds_validate(self):
		self.create_related_tables()
		self.artist_table.ds_validate("name", u"Jonathan")
		self.assertRaises(FieldValidationError,
				self.artist_table.ds_validate, "name", "Jonathan")
		self.artist_table.ds_validate("name",
				self.artist_table.manip(name=u"Jonathan"))


	def test_ds_iter(self):
		self.create_and_insert_related_tables()

		l = [label for value, label in self.band_table.ds_iter()]
		self.assert_("Metallica" in l)

		self.band_table.ds_iter_setup("%(name)s - %(lead_singer)s",
				order_by=[Desc("name")])
		l = [(value, label) for value, label in self.band_table.ds_iter()]
		self.assertEquals(l[1][1], "Metallica - James")
