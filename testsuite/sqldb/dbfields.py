# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase

from enkel.wansgli.testhelpers import unit_case_suite, run_suite
from enkel.sqldb.dbfields import *


class TestDbFields(TestCase):
	def _test_minmax(self, cls, bit):
		n = cls()
		self.assertRaises(FieldValidationError, n.validate, "n", "10")
		self.assertRaises(FieldValidationError, n.validate, "n", None)
		n.validate("n", 10)
		cls(required=False).validate("n", None)

		mmax = 2**(bit-1) - 1
		mmin = -mmax

		n.validate("n", mmin)
		self.assertRaises(FieldValidationError, n.validate, "n", mmin-1)

		n.validate("n", mmax)
		self.assertRaises(FieldValidationError, n.validate, "n", mmax+1)

	def testDbSmallInt(self):
		self._test_minmax(DbSmallInt, 16)
	def testDbInt(self):
		self._test_minmax(DbInt, 32)
	def testDbLong(self):
		self._test_minmax(DbLong, 64)




def suite():
	return unit_case_suite(TestDbFields)

if __name__ == '__main__':
	run_suite(suite())
