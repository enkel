# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase
from cStringIO import StringIO

from enkel.wansgli.testhelpers import unit_case_suite, run_suite
from enkel.sqldb.query import *



class TestQuery(TestCase):
	def setUp(self):
		self.name = QueryField("name", "artists")
		self.age = QueryField("age", "artists")
		self.bname = QueryField("name", "band")

	def test_paramgens(self):
		for paramgen, expected in (
				(paramgen_qmark, "?"),
				(paramgen_numeric, ":9"),
				(paramgen_named, ":p8"),
				(paramgen_format, "%s"),
				(paramgen_pyformat, "%(p8)s")
		):
			self.assertEquals(paramgen(8), expected)

	def test_params_as_dict(self):
		self.assertEquals(
			params_as_dict([10, "yes", "Peter", 20]),
			dict(p0=10, p1="yes", p2="Peter", p3=20))


	def test_op(self):
		p = []
		args = None, paramgen_named, p

		q = OneValueOp(self.name, "=", "Reginald").get_sql(*args)
		self.assertEquals("artists.name = :p0", q)
		self.assertEquals(p, ["Reginald"])

		q = SimpleOp(self.name, "NOTNULL").get_sql(*args)
		self.assertEquals("artists.name NOTNULL", q)
		self.assertEquals(p, ["Reginald"])

		q = InOp(self.name, "", "Reginald", "Peter", "Amy").get_sql(*args)
		self.assertEquals("artists.name IN(:p1,:p2,:p3)", q)
		self.assertEquals(p, ["Reginald", "Reginald", "Peter", "Amy"])

		q = BetweenOp(self.age, "", 10, 20).get_sql(*args)
		self.assertEquals("artists.age BETWEEN :p4 AND :p5", q)
		self.assertEquals(p, ["Reginald", "Reginald", "Peter", "Amy", 10, 20])


	def test_QueryField(self):
		p = []
		args = None, paramgen_qmark, p

		for op, expected in (
				("__eq__", "="),
				("__ne__", "!="),
				("__ge__", ">="),
				("__gt__", ">"),
				("__le__", "<="),
				("__lt__", "<"),
		):
			q = getattr(self.name, op)("a").get_sql(*args)
			self.assertEquals(q, "artists.name %s ?" % expected)
		for x in p:
			self.assertEquals(x, "a")

		p[:] = []
		for op in "startswith", "endswith", "contains":
			f = getattr(self.name, op)
			self.assertEquals(f("j_j").get_sql(*args), 
					"artists.name LIKE ? ESCAPE '|'")
			f = getattr(self.name, "not_" + op)
			self.assertEquals(f("%ja").get_sql(*args), 
					"artists.name NOT LIKE ? ESCAPE '|'")
		self.assertEquals(p,
			['j|_j%', '|%ja%', '%j|_j', '%|%ja', '%j|_j%', '%|%ja%'])

		p[:] = []
		self.assertEquals(self.name.isnull().get_sql(*args),
				"artists.name IS NULL")
		self.assertEquals(self.name.not_null().get_sql(*args),
				"artists.name IS NOT NULL")

		self.assertEquals(self.age.between(10, 20).get_sql(*args),
				"artists.age BETWEEN ? AND ?")
		self.assertEquals(self.age.not_between(10, 20).get_sql(*args),
				"artists.age NOT BETWEEN ? AND ?")

		self.assertEquals(self.name.in_list("x", "y", "z").get_sql(*args),
				"artists.name IN(?,?,?)")
		self.assertEquals(self.name.not_in_list("x", "y", "z").get_sql(*args),
				"artists.name NOT IN(?,?,?)")

		self.assertEquals(p, [10, 20, 10, 20, "x", "y", "z", "x", "y", "z"])


	def test_OpList(self):
		x, p = And(self.name == "Reginald", self.age>=10).compile(
				None, paramgen_qmark)
		self.assertEquals(x, "artists.name = ? AND artists.age >= ?")
		self.assertEquals(p, ["Reginald", 10])

		x, p = Or(
				self.name == "Reginald", self.age>=10,
				And(self.bname.in_list("Jonathan", "Bender"), self.age < 3),
				id=25
			).compile("<name of table>", paramgen_qmark)
		self.assertEquals(x, 
			"artists.name = ? OR artists.age >= ? OR "\
			"(band.name IN(?,?) AND artists.age < ?) OR <name of table>.id = ?")
		self.assertEquals(p, ["Reginald", 10, "Jonathan", "Bender", 3, 25])


	def test_WhereClause(self):

		# sanity check
		e, p = WhereClause("mytable", paramgen_format,
				self.bname == "Korn", name=10).compile()
		self.assertEquals(e, "WHERE band.name = %s AND mytable.name = %s")
		self.assertEquals(p, ["Korn", 10])

		# "And" forwarding check
		self.assertEquals(e[6:],
			And(self.bname == "Korn", name=10).compile(
				"mytable", paramgen_format)[0])

		# Test extra keywords
		e, p = WhereClause("mytable", paramgen_format,
				order_by=[Asc(self.bname), Desc("rating")],
				limit=10, offset=2).compile()
		self.assertEquals(e,
			"ORDER BY band.name ASC, mytable.rating DESC LIMIT 10 OFFSET 2")


def suite():
	return unit_case_suite(TestQuery)

if __name__ == '__main__':
	run_suite(suite())
