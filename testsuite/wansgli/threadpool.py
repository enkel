# This file is part of the Enkel web programming library.
#
# Copyright (C) 2007 Espen Angell Kristiansen (espeak@users.sourceforge.net)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from unittest import TestCase
from cStringIO import StringIO
from threading import Timer

from enkel.wansgli.threadpool import ThreadPool, ThreadPoolClosedError
from enkel.wansgli.testhelpers import unit_case_suite, run_suite


def test1():
	pass
def test2(sec, msg, buf):
	t = Timer(sec, buf.write, [msg])
	t.start()
	t.join()


class TestThreadPool(TestCase):
	""" Tests the entire threadpool module. """

	def test_simple(self):
		p = ThreadPool(10)
		p.add_thread(test1)
		p.join()

	def test_advanced(self):
		buf = StringIO()
		p = ThreadPool(10)
		p.add_thread(test2, 0.5, "1", buf)
		p.add_thread(test1)
		p.add_thread(test2, 0.3, "2", buf)
		p.add_thread(test2, 0.1, "3", buf)
		p.join()
		self.assertEquals(buf.getvalue(), "321")

	def test_add_after_close(self):
		p = ThreadPool(1)
		p.close()
		self.assertRaises(ThreadPoolClosedError, p.add_thread, test1)

	def test_fill_pool(self):
		buf = StringIO()
		p = ThreadPool(1)
		p.add_thread(test2, 0.3, "1", buf)
		p.add_thread(test2, 0.2, "2", buf)
		p.add_thread(test2, 0.1, "3", buf)
		p.join()
		self.assertEquals(buf.getvalue(), "123")



def suite():
	return unit_case_suite(TestThreadPool)

if __name__ == '__main__':
	run_suite(suite())
